package com.cyo.app.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.provider.Settings;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.cyo.app.R;
import com.cyo.app.adapter.HomeAdapter;
import com.cyo.app.adapter.MyListAdapter;
import com.cyo.app.model.BlockOutput;
import com.cyo.app.model.DoctorLoginOutput;
import com.cyo.app.model.GetPatientOutput;
import com.cyo.app.utils.Constant;
import com.cyo.app.utils.GeneralFunction;
import com.cyo.app.utils.Prefs;
import com.cyo.app.webservices.RestClient;
import com.github.javiersantos.appupdater.AppUpdater;
import com.github.javiersantos.appupdater.DisableClickListener;
import com.github.javiersantos.appupdater.enums.Display;
import com.github.javiersantos.appupdater.enums.UpdateFrom;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {
    @BindView(R.id.add_patient_btn)
    public Button add_patient_btn;
    @BindView(R.id.search_txt)
    public EditText search_txt;
    @BindView(R.id.search_no_data)
    public TextView search_no_data;
  @BindView(R.id.parent_lay)
    public LinearLayout parent_lay;

    @BindView(R.id.profile_image)
    public ImageView profile_image;
    @BindView(R.id.no_data_txt)
    public LinearLayout no_data_txt;
    Prefs prefs;
    HomeAdapter adapter;
    RecyclerView recyclerView;
    DoctorLoginOutput doctorLoginOutput;
    ArrayList<GetPatientOutput.UserDetail> patient_list;
    private SwipeRefreshLayout swipeContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        iinit();
        click();
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                getPatientsApi();

                new Handler().postDelayed(new Runnable() {
                    @Override public void run() {
                        // Stop animation (This will be after 3 seconds)
                        swipeContainer.setRefreshing(false);
                    }
                }, 2000);
            }
        });


    }

    private void click() {
        add_patient_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HomeActivity.this,AddPatientActivity.class);
                startActivity(intent);
            }
        });
        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HomeActivity.this,SettingsActivity.class);
                startActivity(intent);
            }
        });

        search_txt.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() ==KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    searchPatientsApi(search_txt.getText().toString());
                    GeneralFunction.hideKeyboard(HomeActivity.this);
                    return true;
                }


                return false;
            }
        });

        search_txt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length()==0)
                {
                    GeneralFunction.hideKeyboard(HomeActivity.this);
                    if(GeneralFunction.isNetworkConnected(HomeActivity.this,parent_lay))
                    {


                        searchPatientsApi("");}
                }
            }
        });
    }

    private void iinit() {
        doctorLoginOutput = Prefs.with(HomeActivity.this).getObject(Constant.USER_DATA, DoctorLoginOutput.class);


        prefs = Prefs.with(HomeActivity.this);
        patient_list=new ArrayList<GetPatientOutput.UserDetail>();
          recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        // Glide.with(HomeActivity.this).load(doctorLoginOutput.userDetail.image).placeholder(R.drawable.placeholder).into(profile_image);

        adapter = new HomeAdapter(HomeActivity.this,patient_list);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager manager = new GridLayoutManager(this, 1, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        // recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        forceUpdateApp();
        if(GeneralFunction.isNetworkConnected(HomeActivity.this,parent_lay))
        {
            blockStatusApi();
        getPatientsApi();}
    }
    private void forceUpdateApp(){
        AppUpdater appUpdater=new AppUpdater(HomeActivity.this);
        //var appUpdater: AppUpdater = AppUpdater(this)
        appUpdater.setDisplay(Display.DIALOG);
        appUpdater.setContentOnUpdateAvailable("Please update new version of CYO to enjoy the new features.");
        appUpdater.setCancelable(false);
        appUpdater.setUpdateFrom(UpdateFrom.GOOGLE_PLAY);
        appUpdater.setButtonDismiss("");
        appUpdater.setButtonDoNotShowAgain("");
        appUpdater.setButtonDoNotShowAgainClickListener(new DisableClickListener(HomeActivity.this));
        appUpdater.setButtonDismissClickListener(new DisableClickListener(HomeActivity.this));
        appUpdater.start();
    }
    private void  getPatientsApi() {

        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("user_id", String.valueOf(doctorLoginOutput.userDetail.userId));


        Log.e("get patient params",""+hashMap);

        GeneralFunction.showProgress(HomeActivity.this);
        RestClient.getModalApiService().apiGetPatient(hashMap).enqueue(new Callback<GetPatientOutput>() {
            @Override
            public void onResponse(Call<GetPatientOutput> call, Response<GetPatientOutput> response) {
                GeneralFunction.dismissProgress();
              //  swipeContainer.setRefreshing(false);

                if (response.isSuccessful()) {
                    Log.e("login res",""+new Gson().toJson(response.body()));

                    patient_list.clear();
                    patient_list.addAll(response.body().userDetails);
                    Collections.reverse(patient_list);
if(patient_list.size()==0)
{
    no_data_txt.setVisibility(View.VISIBLE);
}
else
{
    no_data_txt.setVisibility(View.GONE);
}
                    adapter.notifyDataSetChanged();
                /*    Intent intent=new Intent(HomeActivity.this,HomeActivity.class);
                    startActivity(intent);
                    finish();*/
                 //   Log.e("login res",""+new Gson().toJson(response.body()));
                   // Toast.makeText(LoginActivity.this, ""+response.body().message, Toast.LENGTH_SHORT).show();
                           /* if(view!=null) {
                                view.signupSuccess(response.body().getData());
                            }*/

                }
                else {

                    if(patient_list.size()==0)
                    {
                        no_data_txt.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        no_data_txt.setVisibility(View.GONE);
                    }
                        /*    if(response.code() == UnAuthorized)
                            {
                                if(view!=null) {
                                    view.sessionExpired();
                                }
                            }
                            else {
                                try {
                                    if(view!=null) {
                                        view.signupFailure(new JSONObject(response.errorBody().string()).getString("message"));
                                    }
                                } catch (JSONException | IOException e) {
                                    e.printStackTrace();
                                }
                            }*/
                }
            }

            @Override
            public void onFailure(Call<GetPatientOutput> call, Throwable t) {
                //  if(view!=null) {
                GeneralFunction.dismissProgress();
                //   view.signupFailure(t.getMessage());
                // }
            }

        });

    }

    private void searchPatientsApi(String name) {

        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("user_id", String.valueOf(doctorLoginOutput.userDetail.userId));
        hashMap.put("name", name);


        Log.e("get patient params",""+hashMap);

   //     GeneralFunction.showProgress(HomeActivity.this);
        RestClient.getModalApiService().apiSearchPatient(hashMap).enqueue(new Callback<GetPatientOutput>() {
            @Override
            public void onResponse(Call<GetPatientOutput> call, Response<GetPatientOutput> response) {
             //   GeneralFunction.dismissProgress();
                if (response.isSuccessful()) {
                    Log.e("login res",""+new Gson().toJson(response.body()));
if(response.body().status==1)
{
                    patient_list.clear();
                    patient_list.addAll(response.body().userDetails);
                    Collections.reverse(patient_list);}
else
{
    patient_list.clear();
}
if(patient_list.size()==0)
{
    no_data_txt.setVisibility(View.GONE);
    search_no_data.setVisibility(View.VISIBLE);
}
else
{
    search_no_data.setVisibility(View.GONE);
    no_data_txt.setVisibility(View.GONE);
}
                    adapter.notifyDataSetChanged();
                /*    Intent intent=new Intent(HomeActivity.this,HomeActivity.class);
                    startActivity(intent);
                    finish();*/
                 //   Log.e("login res",""+new Gson().toJson(response.body()));
                   // Toast.makeText(LoginActivity.this, ""+response.body().message, Toast.LENGTH_SHORT).show();
                           /* if(view!=null) {
                                view.signupSuccess(response.body().getData());
                            }*/

                }
                else {

                    if(patient_list.size()==0)
                    {
                        no_data_txt.setVisibility(View.GONE);
                        search_no_data.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        search_no_data.setVisibility(View.GONE);
                        no_data_txt.setVisibility(View.GONE);
                    }

                }
            }

            @Override
            public void onFailure(Call<GetPatientOutput> call, Throwable t) {

            }

        });

    }



    private void blockStatusApi() {

        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("user_id", String.valueOf(doctorLoginOutput.userDetail.userId));



        Log.e("get patient params",""+hashMap);

   //     GeneralFunction.showProgress(HomeActivity.this);
        RestClient.getModalApiService().apiBlockStatus(hashMap).enqueue(new Callback<BlockOutput>() {
            @Override
            public void onResponse(Call<BlockOutput> call, Response<BlockOutput> response) {
             //   GeneralFunction.dismissProgress();
                if (response.isSuccessful()) {
                    Log.e("login res",""+new Gson().toJson(response.body()));


            if(response.body().doctorStatus.equalsIgnoreCase("1"))
                    {

                        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);


                                //Uncomment the below code to Set the message and title from the strings.xml file
                                builder.setMessage("Your account is blocked by admin!") .setTitle("Block");

                                //Setting message manually and performing action on button click
                                builder.setMessage("Your account is blocked by admin!")
                                        .setCancelable(false)
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Prefs.get().removeAll();
                                              Intent intent=new Intent(HomeActivity.this,LoginActivity.class);
                                              intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                              intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                              intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                              startActivity(intent);
                                            }
                                        });
                                       /* .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                //  Action for 'NO' Button
                                                dialog.cancel();
                                                Toast.makeText(getApplicationContext(),"you choose no action for alertbox",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        });*/
                                //Creating dialog box
                                AlertDialog alert = builder.create();
                                //Setting the title manually
                                alert.setTitle("Block");
                                alert.show();

                    }
                    else if(response.body().doctorStatus.equalsIgnoreCase("2"))
            {}
                    String android_id = Settings.Secure.getString(getContentResolver(),
                            Settings.Secure.ANDROID_ID);
                    if(response.body().device_id.equalsIgnoreCase(android_id))
                    {}
                    else
                    {
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(HomeActivity.this);


                        //Uncomment the below code to Set the message and title from the strings.xml file
                        builder.setMessage("Your account is refreshed by the admin. Please login again!") .setTitle("Alert!");

                        //Setting message manually and performing action on button click
                        builder.setMessage("Your account is refreshed by the admin. Please login again!")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Prefs.get().removeAll();
                                        Intent intent=new Intent(HomeActivity.this,LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                });
                                       /* .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                //  Action for 'NO' Button
                                                dialog.cancel();
                                                Toast.makeText(getApplicationContext(),"you choose no action for alertbox",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        });*/
                        //Creating dialog box
                        android.app.AlertDialog alert = builder.create();
                        //Setting the title manually
                        alert.setTitle("Alert!");
                        alert.show();
                    }

                }
                else {



                }
            }

            @Override
            public void onFailure(Call<BlockOutput> call, Throwable t) {

            }

        });

    }

}
