package com.cyo.app.view;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cyo.app.model.DoctorLoginOutput;
import com.cyo.app.utils.Constant;
import com.cyo.app.utils.FileUtils;
import com.cyo.app.utils.GeneralFunction;
import com.cyo.app.utils.Prefs;
import com.cyo.app.webservices.Send_Image_Private_Chat_Webservice;
import com.github.chrisbanes.photoview.PhotoView;
import com.cyo.app.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import iamutkarshtiwari.github.io.ananas.editimage.EditImageActivity;
import iamutkarshtiwari.github.io.ananas.editimage.ImageEditorIntentBuilder;
import iamutkarshtiwari.github.io.ananas.editimage.utils.BitmapUtils;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class PhotoViewActivity extends AppCompatActivity {
    TextView back_btn, edit_txt, image_name;
    String check;
    public static final int ACTION_REQUEST_EDITIMAGE = 9;
    ImageView imageeee;
    RelativeLayout back_lay;
    public static int CAMERA_PREVIEW_RESULT = 1211;
    PhotoView photo_view;
    private String path;
    public ArrayList<String> edit_images;
    RelativeLayout parent_lay;
    private File dir;

    DoctorLoginOutput doctorLoginOutput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_view);
        edit_images = new ArrayList<>();
        doctorLoginOutput = Prefs.with(PhotoViewActivity.this).getObject(Constant.USER_DATA, DoctorLoginOutput.class);
        check="";
        parent_lay = (RelativeLayout) findViewById(R.id.parent_lay);
        edit_txt = (TextView) findViewById(R.id.edit_txt);
        image_name = (TextView) findViewById(R.id.image_name);
        back_btn = (TextView) findViewById(R.id.back_btn);
        photo_view = (PhotoView) findViewById(R.id.photo_view);
        back_lay = (RelativeLayout) findViewById(R.id.back_lay);
        imageeee = (ImageView) findViewById(R.id.imageeee);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        back_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(PhotoViewActivity.this);
        circularProgressDrawable.setStrokeWidth(5.0f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();
        image_name.setText(getIntent().getStringExtra("image_name"));
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(circularProgressDrawable)
                // .error(R.drawable.user_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();
        Glide.with(PhotoViewActivity.this).load(getIntent().getStringExtra("image")).apply(options).into(photo_view);
      /*  if (getIntent().hasExtra("type")) {

            edit_txt.setVisibility(View.GONE);
        } else {
            edit_txt.setVisibility(View.VISIBLE);

        }*/
        edit_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStoragePermissionGranted()) {

                    if (getIntent().hasExtra("type")) {

                        new DownloadImage().execute(getIntent().getStringExtra("image"));
                    } else {
                   /* path = Environment.getExternalStorageDirectory().getAbsolutePath();
                    dir = new File(getIntent().getStringExtra("image"));
                    new PhotoEditorIntent(PhotoViewActivity.this)
                            .setSourceImagePath(getIntent().getStringExtra("image"))
                            .setExportDir(path)
                            //  .setExportPrefix("PhotoEditorApp_result_")
                            .destroySourceAfterSave(true)

                            .startActivityForResult(CAMERA_PREVIEW_RESULT);*/

                        editImageClick(getIntent().getStringExtra("image"));


                    }
                }

            }
        });

    }
    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                //Log.v(TAG,"Permission is granted");
                return true;
            } else {

                //   Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            // Log.v(TAG,"Permission is granted");
            return true;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            // Log.v(TAG,"Permission: "+permissions[0]+ "was "+grantResults[0]);
            //resume tasks needing this permission


            edit_txt.performClick();

        }
    }
    private void editImageClick(String pathh) {
        File outputFile = FileUtils.genEditFile();
        try {
            Intent intent = new ImageEditorIntentBuilder(this, pathh, outputFile.getAbsolutePath())
                    .withAddText()
                    .withPaintFeature()


                    // .withFilterFeature()
                    .withRotateFeature()
                    .withCropFeature()
                    //.withBrightnessFeature()
                    //  .withSaturationFeature()
                    // .withBeautyFeature()
                    .forcePortrait(true)
                    .build();

            EditImageActivity.start(this, intent, ACTION_REQUEST_EDITIMAGE);
        } catch (Exception e) {
            // Toast.makeText(this, R.string.not_selected, Toast.LENGTH_SHORT).show();
            // Log.e("Demo App", e.getMessage());
        }
    }
    public void saveImage(Context context, Bitmap b, String imageName) {
        FileOutputStream foStream;
        try {
            foStream = context.openFileOutput(imageName, Context.MODE_PRIVATE);
            b.compress(Bitmap.CompressFormat.PNG, 100, foStream);
            foStream.close();


            saveImage(b);
        } catch (Exception e) {
            Log.d("saveImage", "Exception 2, Something went wrong!");
            e.printStackTrace();
        }
    }

    private void saveImage(Bitmap finalBitmap) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "");
        myDir.mkdirs();

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fname = "Shutta_" + timeStamp + ".jpg";

        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.e("f path", file.getAbsolutePath());
        path = Environment.getExternalStorageDirectory().getAbsolutePath();
        dir = new File(file.getAbsolutePath());
      /*  new PhotoEditorIntent(PhotoViewActivity.this)
                .setSourceImagePath(file.getAbsolutePath())
                .setExportDir(path)

                //  .setExportPrefix("PhotoEditorApp_result_")
                .destroySourceAfterSave(true)

                .startActivityForResult(CAMERA_PREVIEW_RESULT);*/


        editImageClick(file.getAbsolutePath());
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {
        private String TAG = "DownloadImage";

        private Bitmap downloadImageBitmap(String sUrl) {
            Bitmap bitmap = null;
            try {
                InputStream inputStream = new URL(sUrl).openStream();   // Download Image from URL
                bitmap = BitmapFactory.decodeStream(inputStream);       // Decode Bitmap
                inputStream.close();
            } catch (Exception e) {
                Log.d(TAG, "Exception 1, Something went wrong!");
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            return downloadImageBitmap(params[0]);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            GeneralFunction.showProgress(PhotoViewActivity.this);
        }

        protected void onPostExecute(Bitmap result) {
            GeneralFunction.dismissProgress();

            saveImage(getApplicationContext(), result, "my_image.png");
        }
    }
    private void handleEditorImage(Intent data) {
        String newFilePath = data.getStringExtra(ImageEditorIntentBuilder.OUTPUT_PATH);
        // boolean isImageEdit = data.getBooleanExtra(EditImageActivity.IS_IMAGE_EDITED, false);

       /* if (isImageEdit) {
            Toast.makeText(PhotoViewActivity.this, "save path :"+ newFilePath, Toast.LENGTH_LONG).show();
        } else {
            newFilePath = data.getStringExtra(ImageEditorIntentBuilder.SOURCE_PATH);

        }
*/
        //  loadImage(newFilePath);
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, android.content.Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == CAMERA_PREVIEW_RESULT) {
            if (!dir.exists()) {
                dir.mkdirs();
            }
           // String path = data.getStringExtra(CameraPreviewActivity.RESULT_IMAGE_PATH);
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + path)));
            Toast.makeText(PhotoViewActivity.this, "Image saved!!", Toast.LENGTH_LONG).show();
            Log.e("result", path + "");

            edit_images.add(path);

            if (getIntent().hasExtra("type")) {
            }
            else
            {
                USBCameraActivity.images.add(path);
                USBCameraActivity.images_name.add(getIntent().getStringExtra("image_name") + "-copy");}
            addScreeningApi("",getIntent().getStringExtra("image_name") + "-copy");


        }
        if (resultCode == RESULT_OK && requestCode == ACTION_REQUEST_EDITIMAGE) {

            String path = data.getStringExtra(ImageEditorIntentBuilder.OUTPUT_PATH);
            // sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + path)));
            //Toast.makeText(PhotoViewActivity.this, "Image saved!!", Toast.LENGTH_LONG).show();
            Log.e("result", path + "");

            edit_images.add(path);
            loadImageFromStorage(path);
            if (getIntent().hasExtra("type")) {
                addScreeningApi("edit",getIntent().getStringExtra("image_name") + "-copy");
            }
            else {

            //    Toast.makeText(PhotoViewActivity.this, "" + path, Toast.LENGTH_SHORT).show();



                if(check.equalsIgnoreCase("yes"))
                {
                    USBCameraActivity.images.add(path);
                    USBCameraActivity.images_name.add(getIntent().getStringExtra("image_name") + "-copy");

                    addScreeningApi("",getIntent().getStringExtra("image_name") + "-copy");

                }
                finish();

            }
        //    finish();
        }
    }
    private void loadImageFromStorage(String path)
    {

        try {
            File f=new File(path, "");
            check="yes";
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
           // ImageView img=(ImageView)findViewById(R.id.imgPicker);
           // img.setImageBitmap(b);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            check="no";
          //  Toast.makeText(PhotoViewActivity.this, "catch", Toast.LENGTH_SHORT).show();
        }

    }
    public void addScreeningApi(String type,String image_name) {

        if (GeneralFunction.isNetworkConnected(PhotoViewActivity.this, parent_lay)) {
            Send_Image_Private_Chat_Webservice send_image_private_chat_webservice = new Send_Image_Private_Chat_Webservice();

            //OUTPUT: Milan,London,New York,San Francisco,
            //Remove last comma

            send_image_private_chat_webservice.sendEditPostRequest(type,edit_images, PhotoViewActivity.this, getIntent().getStringExtra("patient_id"), String.valueOf(doctorLoginOutput.userDetail.userId), getIntent().getStringExtra("screening_no"), GeneralFunction.currentDate(), image_name, parent_lay);


        }

    }


}