package com.cyo.app.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cyo.app.R;
import com.cyo.app.adapter.GalleryAdapter;
import com.cyo.app.adapter.MyListAdapter;

import java.io.File;


public class GalleryActivity extends AppCompatActivity {
TextView back_btn;
    RelativeLayout back_lay;
    public static int CAMERA_PREVIEW_RESULT = 1;
    private  String path;
    private File dir;
    GalleryAdapter adapter;
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        back_btn= (TextView) findViewById(R.id.back_btn);
          recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        back_lay= (RelativeLayout) findViewById(R.id.back_lay);
        back_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter = new GalleryAdapter(GalleryActivity.this,USBCameraActivity.images,USBCameraActivity.images_name,getIntent().getStringExtra("screening_no"),getIntent().getStringExtra("patient_id"));
        recyclerView.setHasFixedSize(true);
        GridLayoutManager manager = new GridLayoutManager(this, 4, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        // recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
       // adapter.notifyDataSetChanged();
    }
}
