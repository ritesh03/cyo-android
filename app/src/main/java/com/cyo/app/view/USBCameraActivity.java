package com.cyo.app.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.usb.UsbDevice;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.cyo.app.R;
import com.cyo.app.model.AddPatientOutput;
import com.cyo.app.model.BlockOutput;
import com.cyo.app.model.DoctorLoginOutput;
import com.cyo.app.model.GetPatientOutput;
import com.cyo.app.utils.Constant;
import com.cyo.app.utils.GeneralFunction;
import com.cyo.app.utils.Prefs;
import com.cyo.app.webservices.RestClient;
import com.cyo.app.webservices.Send_Image_Private_Chat_Webservice;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.gson.Gson;
import com.jiangdg.usbcamera.UVCCameraHelper;
import com.jiangdg.usbcamera.utils.FileUtils;
import com.serenegiant.usb.CameraDialog;
import com.serenegiant.usb.Size;
import com.serenegiant.usb.USBMonitor;
import com.serenegiant.usb.common.AbstractUVCCameraHandler;
import com.serenegiant.usb.widget.CameraViewInterface;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.annotations.Nullable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okio.Buffer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.serenegiant.utils.UriHelper.isDownloadsDocument;
import static com.serenegiant.utils.UriHelper.isExternalStorageDocument;
import static com.serenegiant.utils.UriHelper.isGooglePhotosUri;
import static com.serenegiant.utils.UriHelper.isMediaDocument;

/**
 * UVCCamera use demo
 * <p>
 * Created by jiangdongguo on 2017/9/30.
 */

public class USBCameraActivity extends AppCompatActivity implements CameraDialog.CameraDialogParent, CameraViewInterface.Callback {
    private static final String TAG = "Debug";
    @BindView(R.id.camera_view)
    public View mTextureView;
    @BindView(R.id.toolbar)
    public Toolbar mToolbar;
    @BindView(R.id.seekbar_brightness)
    public SeekBar mSeekBrightness;
    @BindView(R.id.seekbar_contrast)
    public SeekBar mSeekContrast;
    @BindView(R.id.switch_rec_voice)
    public Switch mSwitchVoice;
    @BindView(R.id.camera_icon)
    public Button camera_icon;
    @BindView(R.id.gallery_icon)
    public Button gallery_icon;
    @BindView(R.id.gallery_txt)
    public TextView gallery_txt;
    @BindView(R.id.retake_btn)
    public TextView retake_btn;
    @BindView(R.id.parent_lay)
    public RelativeLayout parent_lay;
    @BindView(R.id.save_btn)
    public Button save_btn;
    @BindView(R.id.save_txt)
    public TextView save_txt;
    @BindView(R.id.check_img)
    public ImageView check_img;
    @BindView(R.id.preview)
    public ImageView preview;
    @BindView(R.id.center_lay)
    public RelativeLayout center_lay;
    @BindView(R.id.back_lay)
    public RelativeLayout back_lay;
    @BindView(R.id.front_lay)
    public RelativeLayout front_lay;
    private static final String SEPARATOR = ",";
    private UVCCameraHelper mCameraHelper;
    private CameraViewInterface mUVCCameraView;
    private AlertDialog mDialog;
    private static final int PERMISSION_REQUEST_CODE = 33;
    private boolean isRequest;
    private boolean isPreview;
    String image_path = "";
    public static ArrayList<String> images;

    public static ArrayList<String> images_temp;
    public static ArrayList<String> images_name;
    public ArrayList<String> images_name_temp;
    ArrayList<Uri> resultUri;
    GetPatientOutput.UserDetail patientDetail;
    DoctorLoginOutput doctorLoginOutput;
    String screening_type="";
    private UVCCameraHelper.OnMyDevConnectListener listener = new UVCCameraHelper.OnMyDevConnectListener() {

        @Override
        public void onAttachDev(UsbDevice device) {
            if (mCameraHelper == null || mCameraHelper.getUsbDeviceCount() == 0) {
                showShortMsg("check no usb camera");
                return;
            }
            // request open permission
            if (!isRequest) {
                isRequest = true;
                if (mCameraHelper != null) {

                    mCameraHelper.requestPermission(0);
                }
            }
        }

        @Override
        public void onDettachDev(UsbDevice device) {
            // close camera
            if (isRequest) {
                isRequest = false;
                mCameraHelper.closeCamera();
                showShortMsg(device.getDeviceName() + " is out");
            }
        }

        @Override
        public void onConnectDev(UsbDevice device, boolean isConnected) {
            if (!isConnected) {
               // showShortMsg("fail to connect,please check resolution params");
                isPreview = false;
            } else {
                isPreview = true;
               // showShortMsg("connecting");
                // initialize seekbar
                // need to wait UVCCamera initialize over
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(2500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Looper.prepare();
                        if (mCameraHelper != null && mCameraHelper.isCameraOpened()) {
                            mSeekBrightness.setProgress(mCameraHelper.getModelValue(UVCCameraHelper.MODE_BRIGHTNESS));
                            mSeekContrast.setProgress(mCameraHelper.getModelValue(UVCCameraHelper.MODE_CONTRAST));
                        }
                        Looper.loop();
                    }
                }).start();
            }
        }

        @Override
        public void onDisConnectDev(UsbDevice device) {
            //showShortMsg("disconnecting");
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usbcamera);
        ButterKnife.bind(this);
        initView();
        image_path = "";
        doctorLoginOutput = Prefs.with(USBCameraActivity.this).getObject(Constant.USER_DATA, DoctorLoginOutput.class);
        patientDetail = new Gson().fromJson(getIntent().getStringExtra("patient_details"), GetPatientOutput.UserDetail.class);
        resultUri = new ArrayList<Uri>();
        images_name = new ArrayList<>();
        images_name_temp = new ArrayList<>();
        images = new ArrayList<>();
        images_temp = new ArrayList<>();
        // step.1 initialize UVCCameraHelper
        mUVCCameraView = (CameraViewInterface) mTextureView;
        mUVCCameraView.setCallback(this);
        mCameraHelper = UVCCameraHelper.getInstance();
        mCameraHelper.setDefaultFrameFormat(UVCCameraHelper.FRAME_FORMAT_YUYV);
        mCameraHelper.initUSBMonitor(this, mUVCCameraView, listener);
        screenTypeDialog();


        mCameraHelper.setOnPreviewFrameListener(new AbstractUVCCameraHandler.OnPreViewResultListener() {
            @Override
            public void onPreviewResult(byte[] nv21Yuv) {

            }
        });

        retake_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                camera_icon.setVisibility(View.VISIBLE);
                center_lay.setVisibility(View.GONE);
                save_btn.setVisibility(View.GONE);
                save_txt.setVisibility(View.GONE);
                retake_btn.setVisibility(View.GONE);
                //  mTextureView.setVisibility(View.VISIBLE);

            }
        });
        back_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        gallery_icon.setVisibility(View.GONE);
        gallery_txt.setVisibility(View.GONE);
        gallery_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gallery_icon.performClick();
            }
        });
        gallery_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (images.size() == 0) {
                    Toast.makeText(USBCameraActivity.this, "No images found", Toast.LENGTH_SHORT).show();
                } else {
                    int screen_no=0;
                    if(patientDetail.last_screening_id.equalsIgnoreCase(""))
                    {
                        screen_no=1;
                    }
                    else {
                        if (screening_type.equalsIgnoreCase("new")) {
                            screen_no = Integer.valueOf(patientDetail.last_screening_id);
                            screen_no++;
                        } else {
                            screen_no = Integer.valueOf(patientDetail.last_screening_id);
                        }
                    }
                    Intent intent = new Intent(USBCameraActivity.this, GalleryActivity.class);
                    intent.putExtra("patient_id",getIntent().getStringExtra("patient_id"));
                    intent.putExtra("screening_no",String.valueOf(screen_no));
                    startActivity(intent);
                }
               /* Intent intent = new Intent(USBCameraActivity.this, PhotoViewActivity.class);
                startActivity(intent);*/
            }
        });
        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (image_path.equalsIgnoreCase("")) {
                    Toast.makeText(USBCameraActivity.this, "Please click image", Toast.LENGTH_SHORT).show();
                } else {
                    imageNameDialog();
                }
                //addScreeningApi();
                // addScreeningApiNew();
            }
        });   save_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (image_path.equalsIgnoreCase("")) {
                    Toast.makeText(USBCameraActivity.this, "Please click image", Toast.LENGTH_SHORT).show();
                } else {
                    imageNameDialog();
                }
                //addScreeningApi();
                // addScreeningApiNew();
            }
        });
        camera_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCameraHelper == null || !mCameraHelper.isCameraOpened()) {
                    showShortMsg("sorry,camera open failed");

                } else {
                    if (!checkPermission()) {

                        requestPermission();

                    } else {
                       /// images_temp.clear();
                        //  showShortMsg("Image saved in gallery");
                        String picPath = UVCCameraHelper.ROOT_PATH + System.currentTimeMillis()
                                + UVCCameraHelper.SUFFIX_JPEG;
                        mCameraHelper.capturePicture(picPath, new AbstractUVCCameraHandler.OnCaptureListener() {
                            @Override
                            public void onCaptureResult(String path) {
                                Log.e(TAG, "save path：" + path);

                                image_path = path;
                                //    check_img.setImageBitmap(getBitmap(path));




                                ///  showShortMsg("Image saved in gallery");
                                // showShortMsg("connecting");


                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        center_lay.setVisibility(View.VISIBLE);
                                        retake_btn.setVisibility(View.VISIBLE);
                                        save_btn.setVisibility(View.VISIBLE);
                                        save_txt.setVisibility(View.VISIBLE);

                                        camera_icon.setVisibility(View.GONE);
                                        Glide.with(USBCameraActivity.this).load(image_path).into(preview);
                                        // UI code goes here
                                    }
                                });
                            }
                        });

                        if (image_path.equalsIgnoreCase("")) {
                        } else {


                            //   mTextureView.setVisibility(View.GONE);

                        }

                    }

                }

            }
        });

    }


    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        //  int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);

        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;


                    if (locationAccepted) {

                        //  camera_icon.performClick();
                    }
                    //Snackbar.make(view, "Permission Granted, Now you can access location data and camera.", Snackbar.LENGTH_LONG).show();
                    else {

                        //  Snackbar.make(view, "Permission Denied, You cannot access location data and camera.", Snackbar.LENGTH_LONG).show();

                    }

                }
        }


    }

    private void initView() {
        setSupportActionBar(mToolbar);

        mSeekBrightness.setMax(100);
        mSeekBrightness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (mCameraHelper != null && mCameraHelper.isCameraOpened()) {
                    mCameraHelper.setModelValue(UVCCameraHelper.MODE_BRIGHTNESS, progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        mSeekContrast.setMax(100);
        mSeekContrast.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (mCameraHelper != null && mCameraHelper.isCameraOpened()) {
                    mCameraHelper.setModelValue(UVCCameraHelper.MODE_CONTRAST, progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        images_temp.clear();
        images_name_temp.clear();
 blockStatusApi();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // step.2 register USB event broadcast
        if (mCameraHelper != null) {
            mCameraHelper.registerUSB();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        // step.3 unregister USB event broadcast
        if (mCameraHelper != null) {
            mCameraHelper.unregisterUSB();
        }
    }


    // example: {640x480,320x240,etc}
    private List<String> getResolutionList() {
        List<Size> list = mCameraHelper.getSupportedPreviewSizes();
        List<String> resolutions = null;
        if (list != null && list.size() != 0) {
            resolutions = new ArrayList<>();
            for (Size size : list) {
                if (size != null) {
                    resolutions.add(size.width + "x" + size.height);
                }
            }
        }
        return resolutions;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FileUtils.releaseFile();
        // step.4 release uvc camera resources
        if (mCameraHelper != null) {
            mCameraHelper.release();
        }
    }

    private void showShortMsg(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public USBMonitor getUSBMonitor() {
        return mCameraHelper.getUSBMonitor();
    }

    @Override
    public void onDialogResult(boolean canceled) {
        if (canceled) {
            showShortMsg("取消操作");
        }
    }


    @Override
    public void onSurfaceCreated(CameraViewInterface view, Surface surface) {
        if (!isPreview && mCameraHelper.isCameraOpened()) {
            mCameraHelper.startPreview(mUVCCameraView);
            isPreview = true;
        }
    }

    @Override
    public void onSurfaceChanged(CameraViewInterface view, Surface surface, int width, int height) {

    }

    @Override
    public void onSurfaceDestroy(CameraViewInterface view, Surface surface) {
        if (isPreview && mCameraHelper.isCameraOpened()) {
            mCameraHelper.stopPreview();
            isPreview = false;
        }
    }

    public void screenTypeDialog() {
        // custom dialog
        final Dialog dialog = new Dialog(USBCameraActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.screening_type_dialog);

        final TextView previous_screening_txtview = (TextView) dialog.findViewById(R.id.previous_screening_txtview);
        final TextView new_screening_txtview = (TextView) dialog.findViewById(R.id.new_screening_txtview);

        TextView new_screening_no = (TextView) dialog.findViewById(R.id.new_screening_no);
        TextView prev_screen_no = (TextView) dialog.findViewById(R.id.prev_screen_no);


        if (patientDetail.last_screening_id.equalsIgnoreCase("")) {

            new_screening_no.setText("New screening number: " + "01");
            prev_screen_no.setVisibility(View.GONE);
            previous_screening_txtview.setVisibility(View.GONE);

        } else {

            prev_screen_no.setText("Previous screening number: " + patientDetail.last_screening_id);
            int screen_no = Integer.valueOf(patientDetail.last_screening_id);
            screen_no++;
            new_screening_no.setText("New screening number: " + screen_no);
        }


        TextView dialogButton = (TextView) dialog.findViewById(R.id.cancel);

        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
        new_screening_txtview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screening_type="new";
                dialog.dismiss();
                front_lay.setVisibility(View.GONE);
            }
        });
        previous_screening_txtview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screening_type="pre";
                dialog.dismiss();
                front_lay.setVisibility(View.GONE);
            }
        });


        dialog.show();
    }


    public void imageNameDialog() {
        // custom dialog
        final Dialog dialog = new Dialog(USBCameraActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.image_name_dialog);

        final String temp_name = "";
        final RadioGroup radio_group = (RadioGroup) dialog.findViewById(R.id.radio_group);
        TextView dialogButton = (TextView) dialog.findViewById(R.id.cancel);
        TextView save_name_txtview = (TextView) dialog.findViewById(R.id.save_name_txtview);

        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();


            }
        });


        save_name_txtview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String temp_namee = "Normal / No reagent";

                if (radio_group.getCheckedRadioButtonId() == R.id.normal) {
                  //  temp_namee = "0";
                    temp_namee = "Normal / No reagent";

                } else if (radio_group.getCheckedRadioButtonId() == R.id.acid) {
                   // temp_namee = "1";
                     temp_namee = "Acetic Acid";
                } else {
                   // temp_namee = "2";
                    temp_namee = "Lugol's lodine";
                }

                gallery_icon.setVisibility(View.VISIBLE);
                gallery_txt.setVisibility(View.VISIBLE);
                resultUri.add(Uri.fromFile(new File(image_path)));
                //resultUri.add(Uri.parse(path));
                images_name.add(temp_namee);
                images_name_temp.add(temp_namee);

                images.add(image_path);
                images_temp.add(image_path);
                retake_btn.setVisibility(View.GONE);
                center_lay.setVisibility(View.GONE);
                mTextureView.setVisibility(View.VISIBLE);
                camera_icon.setVisibility(View.VISIBLE);
                save_btn.setVisibility(View.GONE);
                save_txt.setVisibility(View.GONE);
             /*   if (patientDetail.last_screening_id.equalsIgnoreCase("")) {

                    addScreeningApi("1");
                } else {
                    int screen_no = Integer.valueOf(patientDetail.last_screening_id);
                    screen_no++;
                    addScreeningApi(String.valueOf(screen_no));
                }*/

                dialog.dismiss();
            }
        });


        dialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(images_temp.size()>0)
        {
        if (patientDetail.last_screening_id.equalsIgnoreCase("")) {

            addScreeningApi("1");
        } else {

            int screen_no=0;
            if(screening_type.equalsIgnoreCase("new"))
            {
              screen_no = Integer.valueOf(patientDetail.last_screening_id);
            screen_no++;}
            else
            {
                screen_no = Integer.valueOf(patientDetail.last_screening_id);
            }
            addScreeningApi(String.valueOf(screen_no));
        }}
    }

    public void addScreeningApi(String screening_no) {
        if (images_temp.size() == 0) {
            GeneralFunction.showSnackBar(USBCameraActivity.this, parent_lay, "Please click image again");

        } else {
            if (GeneralFunction.isNetworkConnected(USBCameraActivity.this, parent_lay)) {
                Send_Image_Private_Chat_Webservice send_image_private_chat_webservice = new Send_Image_Private_Chat_Webservice();

                StringBuilder csvBuilder = new StringBuilder();
                for (String city : images_name) {
                    csvBuilder.append(city);
                    csvBuilder.append(SEPARATOR);
                }
                String csv = csvBuilder.toString();
                System.out.println(csv);
                //OUTPUT: Milan,London,New York,San Francisco,
                //Remove last comma
                csv = csv.substring(0, csv.length() - SEPARATOR.length());
                Log.e("list", "" + csv);

                send_image_private_chat_webservice.sendEditPostRequest("",images_temp, USBCameraActivity.this, getIntent().getStringExtra("patient_id"), String.valueOf(doctorLoginOutput.userDetail.userId), screening_no, GeneralFunction.currentDate(), csv, parent_lay);
                resultUri.clear();
                //resultUri.add(Uri.parse(path));
                images_name_temp.clear();

                // images.clear();


            }
        }
    }


    private void blockStatusApi() {

        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("user_id", String.valueOf(doctorLoginOutput.userDetail.userId));



        Log.e("get patient params",""+hashMap);

        //     GeneralFunction.showProgress(HomeActivity.this);
        RestClient.getModalApiService().apiBlockStatus(hashMap).enqueue(new Callback<BlockOutput>() {
            @Override
            public void onResponse(Call<BlockOutput> call, Response<BlockOutput> response) {
                //   GeneralFunction.dismissProgress();
                if (response.isSuccessful()) {
                    Log.e("login res",""+new Gson().toJson(response.body()));


                    if(response.body().doctorStatus.equalsIgnoreCase("1"))
                    {

                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(USBCameraActivity.this);


                        //Uncomment the below code to Set the message and title from the strings.xml file
                        builder.setMessage("Your account is blocked by admin!") .setTitle("Block");

                        //Setting message manually and performing action on button click
                        builder.setMessage("Your account is blocked by admin!")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Prefs.get().removeAll();
                                        Intent intent=new Intent(USBCameraActivity.this,LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                });
                                       /* .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                //  Action for 'NO' Button
                                                dialog.cancel();
                                                Toast.makeText(getApplicationContext(),"you choose no action for alertbox",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        });*/
                        //Creating dialog box
                        android.app.AlertDialog alert = builder.create();
                        //Setting the title manually
                        alert.setTitle("Block");
                        alert.show();

                    }
                    else if(response.body().doctorStatus.equalsIgnoreCase("2"))
                    {}
                    String android_id = Settings.Secure.getString(getContentResolver(),
                            Settings.Secure.ANDROID_ID);
                    if(response.body().device_id.equalsIgnoreCase(android_id))
                    {}
                    else
                    {
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(USBCameraActivity.this);


                        //Uncomment the below code to Set the message and title from the strings.xml file
                        builder.setMessage("Your account is refreshed by the admin. Please login again!") .setTitle("Alert!");

                        //Setting message manually and performing action on button click
                        builder.setMessage("Your account is refreshed by the admin. Please login again!")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Prefs.get().removeAll();
                                        Intent intent=new Intent(USBCameraActivity.this,LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                });
                                       /* .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                //  Action for 'NO' Button
                                                dialog.cancel();
                                                Toast.makeText(getApplicationContext(),"you choose no action for alertbox",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        });*/
                        //Creating dialog box
                        android.app.AlertDialog alert = builder.create();
                        //Setting the title manually
                        alert.setTitle("Alert!");
                        alert.show();
                    }

                }
                else {



                }
            }

            @Override
            public void onFailure(Call<BlockOutput> call, Throwable t) {

            }

        });

    }

}
