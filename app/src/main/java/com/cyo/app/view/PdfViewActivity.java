package com.cyo.app.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.cyo.app.R;
import com.cyo.app.model.AddPatientOutput;
import com.cyo.app.model.DoctorLoginOutput;
import com.cyo.app.model.GetPatientOutput;
import com.cyo.app.utils.Constant;
import com.cyo.app.utils.GeneralFunction;
import com.cyo.app.utils.Prefs;
import com.cyo.app.webservices.RestClient;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PdfViewActivity extends AppCompatActivity {
    @BindView(R.id.back_lay)
    public RelativeLayout back_lay;
    @BindView(R.id.title_txtview)
    public TextView title_txtview;
    @BindView(R.id.pdf_img)
    public ImageView pdf_img;

    @BindView(R.id.select_btn)
    public RelativeLayout select_btn;
    @BindView(R.id.parent_lay)
    public RelativeLayout parent_lay;

    DoctorLoginOutput doctorLoginOutput;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_view);
        doctorLoginOutput = Prefs.with(PdfViewActivity.this).getObject(Constant.USER_DATA, DoctorLoginOutput.class);

           ButterKnife.bind(this);
        title_txtview.setText("View Pdf");
        pdf_img.setBackgroundResource(R.drawable.pdf_img);
      //  Glide.with(this).load(getIntent().getStringExtra("url")).into(pdf_img);
       // generateImageFromPdf(Uri.parse(getIntent().getStringExtra("url")));
        pdf_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pdfUrl=getIntent().getStringExtra("url");
              /*  Intent intent=new Intent(HistoryViewActivity.this,ActivitPdfView.class);
                intent.putExtra("pdfUrl",pdfUrl);
                startActivity(intent);*/

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pdfUrl));
                startActivity(browserIntent);
            }
        });
        back_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        select_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(GeneralFunction.isNetworkConnected(PdfViewActivity.this,parent_lay))
                {
                mailPdfApi();}
            }
        });
    }


    private void mailPdfApi() {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("user_id", String.valueOf(doctorLoginOutput.userDetail.userId));
        hashMap.put("email", getIntent().getStringExtra("email"));
        hashMap.put("pdf_name", getIntent().getStringExtra("pdf_name"));
        hashMap.put("pdf_path", getIntent().getStringExtra("url"));
        hashMap.put("firstname",getIntent().getStringExtra("fname"));
        hashMap.put("lastname", getIntent().getStringExtra("lname"));
        hashMap.put("user_id", String.valueOf(doctorLoginOutput.userDetail.userId));
        hashMap.put("user_type", getIntent().getStringExtra("user_type"));
if(getIntent().getStringExtra("user_type").equalsIgnoreCase("2"))
{

    hashMap.put("doctor_receiver_name", getIntent().getStringExtra("dname"));
}


        Log.e("add patient params", "" + hashMap);

        GeneralFunction.showProgress(PdfViewActivity.this);
        RestClient.getModalApiService().apiMailPdf(hashMap).enqueue(new Callback<AddPatientOutput>() {
            @Override
            public void onResponse(Call<AddPatientOutput> call, Response<AddPatientOutput> response) {
                GeneralFunction.dismissProgress();
                if (response.isSuccessful()) {
                    Log.e("login res", "" + new Gson().toJson(response.body()));

                    if(response.body().status==1)
                    {
                        Toast.makeText(PdfViewActivity.this, "Sent successfully", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        GeneralFunction.showSnackBar(PdfViewActivity.this,parent_lay,response.body().message);
                    }

                } else {


                }
            }

            @Override
            public void onFailure(Call<AddPatientOutput> call, Throwable t) {
                //  if(view!=null) {
                GeneralFunction.dismissProgress();
                //   view.signupFailure(t.getMessage());
                // }
            }

        });

    }


    public final static String FOLDER = Environment.getExternalStorageDirectory() + "/PDF";
    private void saveImage(Bitmap bmp) {
        FileOutputStream out = null;
        try {
            File folder = new File(FOLDER);
            if(!folder.exists())
                folder.mkdirs();
            File file = new File(folder, "PDF.png");
            out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
        } catch (Exception e) {
            //todo with exception
        } finally {
            try {
                if (out != null)
                    out.close();
            } catch (Exception e) {
                //todo with exception
            }
        }
        pdf_img.setImageBitmap(bmp);
    }


}
