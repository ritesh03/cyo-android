package com.cyo.app.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cyo.app.R;
import com.cyo.app.adapter.HomeAdapter;
import com.cyo.app.adapter.PreviousScreeningAdapter;
import com.cyo.app.model.BlockOutput;
import com.cyo.app.model.DoctorLoginOutput;
import com.cyo.app.model.GetPatientOutput;
import com.cyo.app.model.GetScreeningOutput;
import com.cyo.app.utils.Constant;
import com.cyo.app.utils.GeneralFunction;
import com.cyo.app.utils.Prefs;
import com.cyo.app.webservices.RestClient;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PreviousScreeningActivity extends AppCompatActivity {
    @BindView(R.id.back_lay)
    public RelativeLayout back_lay;
    @BindView(R.id.parent_lay)
    public LinearLayout parent_lay;
    @BindView(R.id.title_txtview)
    public TextView title_txtview;
    DoctorLoginOutput doctorLoginOutput;
    @BindView(R.id.no_data_txt)
    public LinearLayout no_data_txt;
    PreviousScreeningAdapter adapter;
    ArrayList<GetScreeningOutput.UserDetail> screening_list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_screening);
        ButterKnife.bind(this);
        screening_list=new ArrayList<>();
        title_txtview.setText("Previous Screenings");
        back_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        doctorLoginOutput = Prefs.with(PreviousScreeningActivity.this).getObject(Constant.USER_DATA, DoctorLoginOutput.class);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView_previous);
        adapter = new PreviousScreeningAdapter(PreviousScreeningActivity.this,screening_list);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        // recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

    }



    private void getScreeningApi() {

        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("user_id", String.valueOf(doctorLoginOutput.userDetail.userId));
        hashMap.put("patient_id", getIntent().getStringExtra("patient_id"));


        Log.e("get patient params",""+hashMap);

        GeneralFunction.showProgress(PreviousScreeningActivity.this);
        RestClient.getModalApiService().apiGetScreening(hashMap).enqueue(new Callback<GetScreeningOutput>() {
            @Override
            public void onResponse(Call<GetScreeningOutput> call, Response<GetScreeningOutput> response) {
                GeneralFunction.dismissProgress();
                if (response.isSuccessful()) {
                    Log.e("login res",""+new Gson().toJson(response.body()));

                    screening_list.clear();
                    screening_list.addAll(response.body().userDetails);
                    Collections.reverse(screening_list);
                    if(screening_list.size()==0)
                    {
                        no_data_txt.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        no_data_txt.setVisibility(View.GONE);
                    }
                    adapter.notifyDataSetChanged();
                /*    Intent intent=new Intent(HomeActivity.this,HomeActivity.class);
                    startActivity(intent);
                    finish();*/
                    //   Log.e("login res",""+new Gson().toJson(response.body()));
                    // Toast.makeText(LoginActivity.this, ""+response.body().message, Toast.LENGTH_SHORT).show();
                           /* if(view!=null) {
                                view.signupSuccess(response.body().getData());
                            }*/

                }
                else {

                    if(screening_list.size()==0)
                    {
                        no_data_txt.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        no_data_txt.setVisibility(View.GONE);
                    }
                        /*    if(response.code() == UnAuthorized)
                            {
                                if(view!=null) {
                                    view.sessionExpired();
                                }
                            }
                            else {
                                try {
                                    if(view!=null) {
                                        view.signupFailure(new JSONObject(response.errorBody().string()).getString("message"));
                                    }
                                } catch (JSONException | IOException e) {
                                    e.printStackTrace();
                                }
                            }*/
                }
            }

            @Override
            public void onFailure(Call<GetScreeningOutput> call, Throwable t) {
                //  if(view!=null) {
                GeneralFunction.dismissProgress();
                //   view.signupFailure(t.getMessage());
                // }
            }

        });

    }



    @Override
    protected void onResume() {
        super.onResume();

        if(GeneralFunction.isNetworkConnected(PreviousScreeningActivity.this,parent_lay))
        { blockStatusApi();
            getScreeningApi();}
    }

    private void blockStatusApi() {

        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("user_id", String.valueOf(doctorLoginOutput.userDetail.userId));



        Log.e("get patient params",""+hashMap);

        //     GeneralFunction.showProgress(HomeActivity.this);
        RestClient.getModalApiService().apiBlockStatus(hashMap).enqueue(new Callback<BlockOutput>() {
            @Override
            public void onResponse(Call<BlockOutput> call, Response<BlockOutput> response) {
                //   GeneralFunction.dismissProgress();
                if (response.isSuccessful()) {
                    Log.e("login res",""+new Gson().toJson(response.body()));


                    if(response.body().doctorStatus.equalsIgnoreCase("1"))
                    {

                        AlertDialog.Builder builder = new AlertDialog.Builder(PreviousScreeningActivity.this);


                        //Uncomment the below code to Set the message and title from the strings.xml file
                        builder.setMessage("Your account is blocked by admin!") .setTitle("Block");

                        //Setting message manually and performing action on button click
                        builder.setMessage("Your account is blocked by admin!")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Prefs.get().removeAll();
                                        Intent intent=new Intent(PreviousScreeningActivity.this,LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                });
                                       /* .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                //  Action for 'NO' Button
                                                dialog.cancel();
                                                Toast.makeText(getApplicationContext(),"you choose no action for alertbox",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        });*/
                        //Creating dialog box
                        AlertDialog alert = builder.create();
                        //Setting the title manually
                        alert.setTitle("Block");
                        alert.show();

                    }
                    else if(response.body().doctorStatus.equalsIgnoreCase("2"))
                    {}

                    String android_id = Settings.Secure.getString(getContentResolver(),
                            Settings.Secure.ANDROID_ID);
                    if(response.body().device_id.equalsIgnoreCase(android_id))
                    {}
                    else
                    {
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(PreviousScreeningActivity.this);


                        //Uncomment the below code to Set the message and title from the strings.xml file
                        builder.setMessage("Your account is refreshed by the admin. Please login again!") .setTitle("Alert!");

                        //Setting message manually and performing action on button click
                        builder.setMessage("Your account is refreshed by the admin. Please login again!")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Prefs.get().removeAll();
                                        Intent intent=new Intent(PreviousScreeningActivity.this,LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                });
                                       /* .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                //  Action for 'NO' Button
                                                dialog.cancel();
                                                Toast.makeText(getApplicationContext(),"you choose no action for alertbox",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        });*/
                        //Creating dialog box
                        android.app.AlertDialog alert = builder.create();
                        //Setting the title manually
                        alert.setTitle("Alert!");
                        alert.show();
                    }
                }
                else {



                }
            }

            @Override
            public void onFailure(Call<BlockOutput> call, Throwable t) {

            }

        });

    }


}
