package com.cyo.app.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.cyo.app.R;
import com.cyo.app.model.AddPatientOutput;
import com.cyo.app.model.BlockOutput;
import com.cyo.app.model.DoctorLoginOutput;
import com.cyo.app.model.GetPatientOutput;
import com.cyo.app.utils.Constant;
import com.cyo.app.utils.GeneralFunction;
import com.cyo.app.utils.Prefs;
import com.cyo.app.webservices.RestClient;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;

import adil.dev.lib.materialnumberpicker.dialog.NumberPickerDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientDetailActivity extends AppCompatActivity {
    @BindView(R.id.back_lay)
    public RelativeLayout back_lay;
    @BindView(R.id.title_txtview)
    public TextView title_txtview;
    @BindView(R.id.prev_screen_txtview)
    public TextView prev_screen_txtview;
    @BindView(R.id.new_screen_txtview)
    public TextView new_screen_txtview;
    @BindView(R.id.patient_name)
    public TextView patient_name;
    @BindView(R.id.patient_age)
    public TextView patient_age;
    @BindView(R.id.patient_gender)
    public TextView patient_gender;
    @BindView(R.id.patient_number)
    public TextView patient_number;
    @BindView(R.id.patient_email)
    public TextView patient_email;
    @BindView(R.id.select_txtview)
    public TextView select_txtview;
    @BindView(R.id.update_btn)
    public TextView update_btn;
    @BindView(R.id.patient_lname_edt)
    public TextView patient_lname_edt;
    @BindView(R.id.patient_fname_edt)
    public TextView patient_fname_edt;
    @BindView(R.id.profile_img)
    public CircleImageView profile_img;
    @BindView(R.id.parent_lay)
    public RelativeLayout parent_lay;
    GetPatientOutput.UserDetail userDetail;
    Uri resultUri;
    DoctorLoginOutput doctorLoginOutput;
    String age = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_detail);
        doctorLoginOutput = Prefs.with(PatientDetailActivity.this).getObject(Constant.USER_DATA, DoctorLoginOutput.class);

        userDetail=new Gson().fromJson(getIntent().getStringExtra("patient_details"), GetPatientOutput.UserDetail.class);
         ButterKnife.bind(this);
        select_txtview.setVisibility(View.VISIBLE);

        select_txtview.setText("Edit");
        title_txtview.setText("Patient Details");
        back_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        patient_email.setText(userDetail.email);
        patient_number.setText(userDetail.mobile);
        if(userDetail.gender.equalsIgnoreCase("1"))
        {
            patient_gender.setText("Female");
        }
        else
        {
            patient_gender.setText("Male");
        }

        patient_age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                NumberPickerDialog dialog = new NumberPickerDialog(PatientDetailActivity.this, 1, 100, new NumberPickerDialog.NumberPickerCallBack() {
                    @Override
                    public void onSelectingValue(int value) {
                        // Toast.makeText(AddPatientActivity.this, "Selected "+String.valueOf(value), Toast.LENGTH_SHORT).show();

                        patient_age.setText(String.valueOf(value) + " years");
                        age = String.valueOf(value);
                    }
                });
                dialog.show();
            }
        });
        patient_age.setText(userDetail.age+" years");
        age=userDetail.age;
        patient_name.setText(userDetail.firstname+" "+userDetail.lastname);
        patient_fname_edt.setText(userDetail.firstname);
        patient_lname_edt.setText(userDetail.lastname);
        Glide.with(PatientDetailActivity.this).load(userDetail.image).placeholder(R.drawable.placeholder_woman).into(profile_img);

        profile_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(select_txtview.getText().toString().equalsIgnoreCase("edit"))
                {}
                else {
                    CropImage.activity()
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .start(PatientDetailActivity.this);
                }
            }
        });
        prev_screen_txtview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PatientDetailActivity.this,PreviousScreeningActivity.class);
                intent.putExtra("patient_id",userDetail.id);
                startActivity(intent);
            }
        });
        update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GeneralFunction.hideKeyboard(PatientDetailActivity.this);
                if(validationCheck())
                {
                    if (resultUri != null) {
                        if(GeneralFunction.isNetworkConnected(PatientDetailActivity.this,parent_lay))
                        {
                            editpatientWithImageApi();
                        }
                    } else {

                        if(GeneralFunction.isNetworkConnected(PatientDetailActivity.this,parent_lay))
                        {
                            editPatientsApi();}
                    }


                    }
            }
        });



        select_txtview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(select_txtview.getText().toString().equalsIgnoreCase("edit"))
                {
                    select_txtview.setText("Cancel");
                    prev_screen_txtview.setVisibility(View.GONE);
                    new_screen_txtview.setVisibility(View.GONE);
                    update_btn.setVisibility(View.VISIBLE);
                  //  profile_img.setEnabled(true);
                    patient_age.setEnabled(true);
                    patient_name.setEnabled(true);
                    patient_number.setEnabled(true);
                    patient_lname_edt.setEnabled(true);
                    patient_fname_edt.setEnabled(true);
                    patient_email.setEnabled(true);
                }
                else
                {
                    GeneralFunction.hideKeyboard(PatientDetailActivity.this);
                    prev_screen_txtview.setVisibility(View.VISIBLE);
                    new_screen_txtview.setVisibility(View.VISIBLE);
                    update_btn.setVisibility(View.GONE);
                    profile_img.setEnabled(false);
                    patient_age.setEnabled(false);
                    patient_number.setEnabled(false);
                    patient_lname_edt.setEnabled(false);
                    patient_fname_edt.setEnabled(false);
                    patient_email.setEnabled(false);
                    patient_name.setEnabled(false);
                    select_txtview.setText("Edit");
                }
            }
        });
        new_screen_txtview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PatientDetailActivity.this,USBCameraActivity.class);
                intent.putExtra("patient_id",userDetail.id);
                intent.putExtra("patient_details",new Gson().toJson(userDetail));
                startActivity(intent);
            }
        });
    }
    private void editPatientsApi() {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("user_id", String.valueOf(doctorLoginOutput.userDetail.userId));
        hashMap.put("patient_id", String.valueOf(userDetail.id));
        hashMap.put("firstname", patient_fname_edt.getText().toString().trim());
        hashMap.put("lastname", patient_lname_edt.getText().toString().trim());
        hashMap.put("gender", "1");
        hashMap.put("email", patient_email.getText().toString().trim());
        hashMap.put("mobile", patient_number.getText().toString().trim());
        hashMap.put("age", age);


        Log.e("add patient params", "" + hashMap);

        GeneralFunction.showProgress(PatientDetailActivity.this);
        RestClient.getModalApiService().apiUpdate_patient(hashMap).enqueue(new Callback<AddPatientOutput>() {
            @Override
            public void onResponse(Call<AddPatientOutput> call, Response<AddPatientOutput> response) {
                GeneralFunction.dismissProgress();
                if (response.isSuccessful()) {
                    Log.e("login res", "" + new Gson().toJson(response.body()));

                    if(response.body().status==1)
                    {
                        Toast.makeText(PatientDetailActivity.this, "Patient updated successfully", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        GeneralFunction.showSnackBar(PatientDetailActivity.this,parent_lay,response.body().message);
                    }

                } else {


                }
            }

            @Override
            public void onFailure(Call<AddPatientOutput> call, Throwable t) {
                //  if(view!=null) {
                GeneralFunction.dismissProgress();
                //   view.signupFailure(t.getMessage());
                // }
            }

        });

    }

    private void editpatientWithImageApi() {
        GeneralFunction.showProgress(PatientDetailActivity.this);

        MultipartBody.Part imageUser = null;
        if (resultUri != null) {
            final RequestBody reqFile;
            try {
                reqFile = RequestBody.create(MediaType.parse("image/*"), new File(new URI(resultUri.toString())));
                imageUser = MultipartBody.Part.createFormData("image", new File(new URI(resultUri.toString())).getName(), reqFile);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

        }

        RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(doctorLoginOutput.userDetail.userId));
        RequestBody patient_id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userDetail.id));
        RequestBody firstname = RequestBody.create(MediaType.parse("text/plain"), patient_fname_edt.getText().toString().trim());
        RequestBody lastname = RequestBody.create(MediaType.parse("text/plain"), patient_lname_edt.getText().toString().trim());
        RequestBody gender = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), patient_email.getText().toString().trim());
        RequestBody mobile = RequestBody.create(MediaType.parse("text/plain"), patient_number.getText().toString().trim());
        RequestBody agee = RequestBody.create(MediaType.parse("text/plain"), age);



        RestClient.getModalApiService().apiUpdatePatientWithImage(imageUser, user_id, patient_id,firstname, lastname, gender, email, mobile, agee).enqueue(new Callback<AddPatientOutput>() {
            @Override
            public void onResponse(Call<AddPatientOutput> call, Response<AddPatientOutput> response) {


                GeneralFunction.dismissProgress();

                if(response.body().status==1)
                {
                    Toast.makeText(PatientDetailActivity.this, "Patient updated successfully", Toast.LENGTH_SHORT).show();
                    finish();
                }
                else
                {
                    GeneralFunction.showSnackBar(PatientDetailActivity.this,parent_lay,response.body().message);
                }

                Log.e("res", "" + new Gson().toJson(response.body()));
            }

            @Override
            public void onFailure(Call<AddPatientOutput> call, Throwable t) {
                //  commonFunction.hideProgressDialog();
                GeneralFunction.dismissProgress();
            }
        });

    }

    private boolean validationCheck() {


        String firstName = patient_fname_edt.getText().toString().trim();

        String lastName=patient_lname_edt.getText().toString().trim();;

        String age = this.age;
        String phoneNo = patient_number.getText().toString().trim();
        String email = patient_email.getText().toString().trim();

        if (firstName.isEmpty()) {
            GeneralFunction.showSnackBar(PatientDetailActivity.this, parent_lay, "Please enter first name");

            return false;
        }   else if (lastName.isEmpty()) {
            GeneralFunction.showSnackBar(PatientDetailActivity.this, parent_lay, "Please enter last name");
            return false;
        }   else if (age.isEmpty()) {
            GeneralFunction.showSnackBar(PatientDetailActivity.this, parent_lay, "Please enter age");

            return false;
        }
        else if (phoneNo.isEmpty()) {
            GeneralFunction.showSnackBar(PatientDetailActivity.this, parent_lay, "Please enter contact number");
            return false;
        } else if (phoneNo.length()<10) {
            GeneralFunction.showSnackBar(PatientDetailActivity.this, parent_lay, "Please enter valid contact number");
            return false;
        }

        else if (!email.isEmpty()&&!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            GeneralFunction.showSnackBar(PatientDetailActivity.this, parent_lay, "Please enter valid email");

            return false;
        }




        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        blockStatusApi();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1213 && resultCode == Activity.RESULT_OK) {
          /*  String filePath = data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH);
            Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
            profile_img.setImageBitmap(selectedImage);*/
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                //  String filePath = data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH);
                resultUri = result.getUri();
                Glide.with(PatientDetailActivity.this).load(resultUri).placeholder(R.drawable.placeholder_woman).into(profile_img);

                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //profile_img.setImageBitmap(bitmap);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
    private void blockStatusApi() {

        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("user_id", String.valueOf(doctorLoginOutput.userDetail.userId));



        Log.e("get patient params",""+hashMap);

        //     GeneralFunction.showProgress(HomeActivity.this);
        RestClient.getModalApiService().apiBlockStatus(hashMap).enqueue(new Callback<BlockOutput>() {
            @Override
            public void onResponse(Call<BlockOutput> call, Response<BlockOutput> response) {
                //   GeneralFunction.dismissProgress();
                if (response.isSuccessful()) {
                    Log.e("login res",""+new Gson().toJson(response.body()));


                    if(response.body().doctorStatus.equalsIgnoreCase("1"))
                    {

                        AlertDialog.Builder builder = new AlertDialog.Builder(PatientDetailActivity.this);


                        //Uncomment the below code to Set the message and title from the strings.xml file
                        builder.setMessage("Your account is blocked by admin!") .setTitle("Block");

                        //Setting message manually and performing action on button click
                        builder.setMessage("Your account is blocked by admin!")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Prefs.get().removeAll();
                                        Intent intent=new Intent(PatientDetailActivity.this,LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                });
                                       /* .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                //  Action for 'NO' Button
                                                dialog.cancel();
                                                Toast.makeText(getApplicationContext(),"you choose no action for alertbox",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        });*/
                        //Creating dialog box
                        AlertDialog alert = builder.create();
                        //Setting the title manually
                        alert.setTitle("Block");
                        alert.show();

                    }
                    else if(response.body().doctorStatus.equalsIgnoreCase("2"))
                    {}
                    String android_id = Settings.Secure.getString(getContentResolver(),
                            Settings.Secure.ANDROID_ID);
                    if(response.body().device_id.equalsIgnoreCase(android_id))
                    {}
                    else
                    {
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(PatientDetailActivity.this);


                        //Uncomment the below code to Set the message and title from the strings.xml file
                        builder.setMessage("Your account is refreshed by the admin. Please login again!") .setTitle("Alert!");

                        //Setting message manually and performing action on button click
                        builder.setMessage("Your account is refreshed by the admin. Please login again!")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Prefs.get().removeAll();
                                        Intent intent=new Intent(PatientDetailActivity.this,LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                });
                                       /* .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                //  Action for 'NO' Button
                                                dialog.cancel();
                                                Toast.makeText(getApplicationContext(),"you choose no action for alertbox",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        });*/
                        //Creating dialog box
                        android.app.AlertDialog alert = builder.create();
                        //Setting the title manually
                        alert.setTitle("Alert!");
                        alert.show();
                    }

                }
                else {



                }
            }

            @Override
            public void onFailure(Call<BlockOutput> call, Throwable t) {

            }

        });

    }
}
