package com.cyo.app.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.cyo.app.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsActivity extends AppCompatActivity {
    @BindView(R.id.back_lay)
    public RelativeLayout back_lay;
    @BindView(R.id.parent_lay)
    public LinearLayout parent_lay;
    @BindView(R.id.title_txtview)
    public TextView title_txtview;
    @BindView(R.id.profile_txt)
    public TextView profile_txt;
    @BindView(R.id.contact_us_txt)
    public TextView contact_us_txt;
    @BindView(R.id.feedback_txt)
    public TextView feedback_txt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);

        title_txtview.setText("");
        back_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        profile_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SettingsActivity.this,DocProfileActivity.class);
                startActivity(intent);
            }
        });

        contact_us_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SettingsActivity.this,ActivityContactUs.class);
                startActivity(intent);
            }
        });

        feedback_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SettingsActivity.this,WriteFeedbackActivity.class);
                startActivity(intent);
            }
        });

    }
}
