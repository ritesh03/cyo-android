package com.cyo.app.view;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.cyo.app.R;
import com.cyo.app.model.DoctorLoginOutput;
import com.cyo.app.utils.Constant;
import com.cyo.app.utils.GeneralFunction;
import com.cyo.app.utils.Prefs;
import com.cyo.app.webservices.RestClient;
import com.google.gson.Gson;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyActivity extends AppCompatActivity {
    @BindView(R.id.submit_btn)
    public Button submit_btn;
    @BindView(R.id.et_otp)
    public EditText et_otp;

    @BindView(R.id.parent_lay)
    public RelativeLayout parent_lay;
    @BindView(R.id.tv_resend_otp)
    public TextView tv_resend_otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);
        ButterKnife.bind(this);

        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_otp.getText().toString().trim().isEmpty()) {

                    GeneralFunction.showSnackBar(VerifyActivity.this, parent_lay, "Please enter otp");
                }else {
                    verfiyOtp();
                }
            }
        });

        tv_resend_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    resendOtp();

            }
        });
    }

    private void verfiyOtp(){
        GeneralFunction.showProgress(VerifyActivity.this);
        String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.d("UserId", getIntent().getStringExtra("userId"));
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("user_id", getIntent().getStringExtra("userId"));
        hashMap.put("otp", et_otp.getText().toString().trim());

        RestClient.getModalApiService().apiOtpVerify(hashMap).enqueue(new Callback<DoctorLoginOutput>() {
            @Override
            public void onResponse(Call<DoctorLoginOutput> call, Response<DoctorLoginOutput> response) {
                GeneralFunction.dismissProgress();
                if (response.isSuccessful()) {
                    if (response.body().status == 1) {
                        Prefs prefs = Prefs.with(VerifyActivity.this);
                        prefs.save(Constant.USER_DATA, response.body());
                        Intent intent = new Intent(VerifyActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                        Log.e("Otp res", "" + new Gson().toJson(response.body()));
                    } else {
                        Toast.makeText(VerifyActivity.this, "" + response.body().message, Toast.LENGTH_SHORT).show();
                    }


                } else {
                    GeneralFunction.dismissProgress();

                }
            }

            @Override
            public void onFailure(Call<DoctorLoginOutput> call, Throwable t) {

                GeneralFunction.dismissProgress();
            }

        });

    }


    private void resendOtp() {

        String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("user_id", getIntent().getStringExtra("userId"));
        hashMap.put("mobile", getIntent().getStringExtra("mobile"));
        hashMap.put("device_id", android_id);
        hashMap.put("device_type", "A");

        Log.e("login params", "" + hashMap);

        GeneralFunction.showProgress(VerifyActivity.this);
        RestClient.getModalApiService().apiDoctorLogin(hashMap).enqueue(new Callback<DoctorLoginOutput>() {
            @Override
            public void onResponse(Call<DoctorLoginOutput> call, Response<DoctorLoginOutput> response) {
                GeneralFunction.dismissProgress();
                if (response.isSuccessful()) {
                    if (response.body().status == 1) {
                        Toast.makeText(VerifyActivity.this, "OTP has been resent", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(VerifyActivity.this, "" + response.body().message, Toast.LENGTH_SHORT).show();
                    }

                } else {
                    GeneralFunction.dismissProgress();

                }
            }

            @Override
            public void onFailure(Call<DoctorLoginOutput> call, Throwable t) {
                GeneralFunction.dismissProgress();

            }

        });

    }
}
