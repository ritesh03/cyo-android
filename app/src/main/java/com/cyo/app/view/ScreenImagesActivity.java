package com.cyo.app.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager;
import com.cyo.app.R;
import com.cyo.app.adapter.MyListAdapter;
import com.cyo.app.adapter.SampleAdapter;
import com.cyo.app.model.AddPatientOutput;
import com.cyo.app.model.DoctorLoginOutput;
import com.cyo.app.model.ScreeningDetailsOutput;
import com.cyo.app.model.SharePdfOutput;
import com.cyo.app.utils.Constant;
import com.cyo.app.utils.GeneralFunction;
import com.cyo.app.utils.Prefs;
import com.cyo.app.webservices.RestClient;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScreenImagesActivity extends AppCompatActivity {
    private static final String SEPARATOR = ",";
    @BindView(R.id.parent_lay)
    public LinearLayout parent_lay;
    @BindView(R.id.back_lay)
    public RelativeLayout back_lay;
    @BindView(R.id.title_txtview)
    public TextView title_txtview;
    @BindView(R.id.screening_txt)
    public TextView screening_txt;
    @BindView(R.id.select_txtview)
    public TextView select_txtview;
    @BindView(R.id.no_data_txt)
    public LinearLayout no_data_txt;
    boolean visibility_status = false;
    private static final int SPAN_SIZE = 4;
    static int SECTIONS = 3;
    private static final int SECTION_ITEMS = 7;
    RecyclerView recyclerView;
    DoctorLoginOutput doctorLoginOutput;
    MyListAdapter myListAdapter;
    public static ArrayList<String> image_ids;
    public static ImageView share_icon;
    String user_type = "";
    public static ImageView delete_icon;
    public static String img_id="";
    ArrayList<ScreeningDetailsOutput.UserDetail> screening_detail_list;
    private StickyHeaderGridLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_images);
        ButterKnife.bind(this);
        user_type = "";

        share_icon = findViewById(R.id.share_icon);
        delete_icon = findViewById(R.id.delete_icon);
        image_ids = new ArrayList<>();
        screening_detail_list = new ArrayList<>();
        doctorLoginOutput = Prefs.with(ScreenImagesActivity.this).getObject(Constant.USER_DATA, DoctorLoginOutput.class);

        title_txtview.setText(getIntent().getStringExtra("patient_name"));
        select_txtview.setVisibility(View.GONE);
        back_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // custom dialog
                share_popup();
            }
        });

        delete_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // custom dialog
                AlertDialog diaBox = AskOption();
                diaBox.show();
            }
        });

        screening_txt.setText("Screening " + getIntent().getStringExtra("screening_no"));
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_screen);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager manager = new GridLayoutManager(this, 1, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        // recyclerView.setLayoutManager(new LinearLayoutManager(this));

        myListAdapter = new MyListAdapter(ScreenImagesActivity.this, select_txtview, screening_detail_list, visibility_status);
        recyclerView.setAdapter(myListAdapter);

        select_txtview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (select_txtview.getText().toString().equalsIgnoreCase("select")) {
                    select_txtview.setText("Cancel");
                    visibility_status = true;
                } else {
                    select_txtview.setVisibility(View.GONE);
                    select_txtview.setText("Select");
                    img_id="";
                    image_ids.clear();
                    visibility_status = false;
                }

                myListAdapter = new MyListAdapter(ScreenImagesActivity.this, select_txtview, screening_detail_list, visibility_status);
                recyclerView.setAdapter(myListAdapter);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        image_ids.clear();
        visibility_status = false;
        select_txtview.setText("Select");
        select_txtview.setVisibility(View.GONE);
        share_icon.setVisibility(View.GONE);
        delete_icon.setVisibility(View.GONE);
        img_id="";
        myListAdapter = new MyListAdapter(ScreenImagesActivity.this, select_txtview, screening_detail_list, visibility_status);
        recyclerView.setAdapter(myListAdapter);


        if (GeneralFunction.isNetworkConnected(ScreenImagesActivity.this, parent_lay)) {
            getScreeningDetailsApi();
        }
    }

    private void getScreeningDetailsApi() {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("user_id", String.valueOf(doctorLoginOutput.userDetail.userId));
        hashMap.put("patient_id", getIntent().getStringExtra("patient_id"));
        hashMap.put("screening_id", getIntent().getStringExtra("screening_id"));


        Log.e("get patient params", "" + hashMap);

        GeneralFunction.showProgress(ScreenImagesActivity.this);
        RestClient.getModalApiService().apiScreenDetails(hashMap).enqueue(new Callback<ScreeningDetailsOutput>() {
            @Override
            public void onResponse(Call<ScreeningDetailsOutput> call, Response<ScreeningDetailsOutput> response) {
                GeneralFunction.dismissProgress();
                if (response.isSuccessful()) {
                    Log.e("login res", "" + new Gson().toJson(response.body()));
                    screening_detail_list.clear();
                    if (response.body().status == 1) {

                        screening_detail_list.addAll(response.body().userDetails);
                        Collections.reverse(screening_detail_list);
                        List<String> labels = new ArrayList<>();

                        SECTIONS = labels.size();

                    }

                    visibility_status = false;
                    myListAdapter = new MyListAdapter(ScreenImagesActivity.this,
                            select_txtview, screening_detail_list, visibility_status);
                    recyclerView.setAdapter(myListAdapter);

                  //  myListAdapter.notifyDataSetChanged();
                    if (screening_detail_list.size() == 0) {
                        no_data_txt.setVisibility(View.VISIBLE);
                        select_txtview.setVisibility(View.GONE);
                    } else {


                        no_data_txt.setVisibility(View.GONE);
                        select_txtview.setVisibility(View.GONE);
                    }

                } else {

                    if (screening_detail_list.size() == 0) {
                        no_data_txt.setVisibility(View.VISIBLE);
                    } else {
                        no_data_txt.setVisibility(View.GONE);
                    }
                        /*    if(response.code() == UnAuthorized)
                            {
                                if(view!=null) {
                                    view.sessionExpired();
                                }
                            }
                            else {
                                try {
                                    if(view!=null) {
                                        view.signupFailure(new JSONObject(response.errorBody().string()).getString("message"));
                                    }
                                } catch (JSONException | IOException e) {
                                    e.printStackTrace();
                                }
                            }*/
                }
            }

            @Override
            public void onFailure(Call<ScreeningDetailsOutput> call, Throwable t) {
                //  if(view!=null) {
                GeneralFunction.dismissProgress();
                //   view.signupFailure(t.getMessage());
                // }
            }

        });

    }

    private void deleteImagesApi() {

        StringBuilder csvBuilder = new StringBuilder();
        for (String city : image_ids) {
            csvBuilder.append(city);
            csvBuilder.append(SEPARATOR);
        }
        String csv = csvBuilder.toString();
        System.out.println(csv);
        //OUTPUT: Milan,London,New York,San Francisco,
        //Remove last comma
        csv = csv.substring(0, csv.length() - SEPARATOR.length());
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id", csv);


        Log.e("get patient params", "" + hashMap);

        GeneralFunction.showProgress(ScreenImagesActivity.this);
        RestClient.getModalApiService().apiDeleteImages(hashMap).enqueue(new Callback<AddPatientOutput>() {
            @Override
            public void onResponse(Call<AddPatientOutput> call, Response<AddPatientOutput> response) {
                GeneralFunction.dismissProgress();
                if (response.isSuccessful()) {
                    Log.e("login res", "" + new Gson().toJson(response.body()));
                    screening_detail_list.clear();
                    if (response.body().status == 1) {
                        if (GeneralFunction.isNetworkConnected(ScreenImagesActivity.this, parent_lay)) {
                            getScreeningDetailsApi();
                        }

                    }


                } else {


                }
            }

            @Override
            public void onFailure(Call<AddPatientOutput> call, Throwable t) {
                //  if(view!=null) {
                GeneralFunction.dismissProgress();
                //   view.signupFailure(t.getMessage());
                // }
            }

        });

    }

    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
                //set message, title, and icon
                .setTitle("Delete")
                .setMessage("Are you sure you want to delete?")
                .setIcon(R.drawable.ic_delete_white)

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code

                        if (GeneralFunction.isNetworkConnected(ScreenImagesActivity.this, parent_lay)) {
                            deleteImagesApi();
                        }
                        dialog.dismiss();
                    }

                })


                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();
        return myQuittingDialogBox;

    }


    private void share_popup() {
        final Dialog dialog = new Dialog(ScreenImagesActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.share_dialog);


        TextView dialogButton = (TextView) dialog.findViewById(R.id.cancel);
        final TextView share_btn = (TextView) dialog.findViewById(R.id.share_btn);
        final TextView email_placeholder = (TextView) dialog.findViewById(R.id.email_placeholder);
        final EditText email_edittxt = (EditText) dialog.findViewById(R.id.email_edittxt);
        final TextView patient_btn = (TextView) dialog.findViewById(R.id.patient_btn);
        final TextView doctor_btn = (TextView) dialog.findViewById(R.id.doctor_btn);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        share_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user_type.equalsIgnoreCase("2")) {

                    if (email_edittxt.getText().toString().equalsIgnoreCase("")) {
                        GeneralFunction.showSnackBar(ScreenImagesActivity.this, parent_lay, "Please enter email ID");

                    } else if (!Patterns.EMAIL_ADDRESS.matcher(email_edittxt.getText().toString()).matches()) {
                        GeneralFunction.showSnackBar(ScreenImagesActivity.this, parent_lay, "Please enter valid email");


                    } else {
                        Intent intent = new Intent(ScreenImagesActivity.this, Webview_activity.class);

                        intent.putExtra("email", email_edittxt.getText().toString());

                        intent.putExtra("patient_email", getIntent().getStringExtra("patient_email"));
                        intent.putExtra("fname", getIntent().getStringExtra("fname"));
                        intent.putExtra("lname", getIntent().getStringExtra("lname"));
                        intent.putExtra("age", getIntent().getStringExtra("age"));
                        intent.putExtra("mobile", getIntent().getStringExtra("mobile"));
                        intent.putExtra("gender", getIntent().getStringExtra("gender"));
                        startActivity(intent);
                        dialog.dismiss();
                    }


                } else {
                    if (email_edittxt.getText().toString().equalsIgnoreCase("")) {
                        GeneralFunction.showSnackBar(ScreenImagesActivity.this, parent_lay, "Please enter email ID");

                    } else if (!Patterns.EMAIL_ADDRESS.matcher(email_edittxt.getText().toString()).matches()) {
                        GeneralFunction.showSnackBar(ScreenImagesActivity.this, parent_lay, "Please enter valid email");


                    } else {
                        sharePdfApi(email_edittxt.getText().toString());
                        dialog.dismiss();
                    }
                }

            }
        });

        patient_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                user_type = "2";
                share_btn.setVisibility(View.VISIBLE);
                email_placeholder.setVisibility(View.VISIBLE);
                email_edittxt.setVisibility(View.VISIBLE);
                patient_btn.setVisibility(View.GONE);
                doctor_btn.setVisibility(View.GONE);
                // email_edittxt.setEnabled(false);
                email_edittxt.setText(getIntent().getStringExtra("patient_email"));
            }
        });
        doctor_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                user_type = "2";
                share_btn.setVisibility(View.VISIBLE);
                email_placeholder.setVisibility(View.VISIBLE);
                email_edittxt.setVisibility(View.VISIBLE);
                patient_btn.setVisibility(View.GONE);
                doctor_btn.setVisibility(View.GONE);
                email_edittxt.setEnabled(true);
                email_edittxt.setText("");

            }
        });

        dialog.show();
    }


    private void sharePdfApi(String email) {
        StringBuilder csvBuilder = new StringBuilder();
        for (String city : image_ids) {
            csvBuilder.append(city);
            csvBuilder.append(SEPARATOR);
        }
        String csv = csvBuilder.toString();
        System.out.println(csv);
        //OUTPUT: Milan,London,New York,San Francisco,
        //Remove last comma
        csv = csv.substring(0, csv.length() - SEPARATOR.length());
        Log.e("list", "" + csv);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("user_id", String.valueOf(doctorLoginOutput.userDetail.userId));
        hashMap.put("user_type", "1");
        hashMap.put("email", email);
        hashMap.put("image_ids", csv);
        hashMap.put("firstname", getIntent().getStringExtra("fname"));
        hashMap.put("lastname", getIntent().getStringExtra("lname"));


        Log.e("share patient params", "" + hashMap);

        GeneralFunction.showProgress(ScreenImagesActivity.this);
        RestClient.getModalApiService().apiSharePdf(hashMap).enqueue(new Callback<SharePdfOutput>() {
            @Override
            public void onResponse(Call<SharePdfOutput> call, Response<SharePdfOutput> response) {
                GeneralFunction.dismissProgress();
                if (response.isSuccessful()) {
                    Log.e("share res", "" + new Gson().toJson(response.body()));

                    if (response.body().status.equalsIgnoreCase("1")) {
                        Intent intent = new Intent(ScreenImagesActivity.this, PdfViewActivity.class);
                        intent.putExtra("url", response.body().pdfPath);
                        intent.putExtra("email", getIntent().getStringExtra("patient_email"));
                        intent.putExtra("fname", getIntent().getStringExtra("fname"));
                        intent.putExtra("lname", getIntent().getStringExtra("lname"));
                        intent.putExtra("pdf_name", response.body().pdfName);
                        intent.putExtra("user_type", user_type);
                        startActivity(intent);

                    } else {
                        GeneralFunction.showSnackBar(ScreenImagesActivity.this, parent_lay, response.body().message);
                    }

                } else {


                }
            }

            @Override
            public void onFailure(Call<SharePdfOutput> call, Throwable t) {
                //  if(view!=null) {
                GeneralFunction.dismissProgress();
                //   view.signupFailure(t.getMessage());
                // }
            }

        });

    }
}
