package com.cyo.app.view;

import android.content.Intent;
import android.provider.Settings;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.cyo.app.R;
import com.cyo.app.model.DoctorLoginOutput;
import com.cyo.app.utils.Constant;
import com.cyo.app.utils.GeneralFunction;
import com.cyo.app.utils.Prefs;
import com.cyo.app.webservices.RestClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.lang.reflect.Modifier.TRANSIENT;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.mobile_no_edt)
    public EditText mobile_no_edt;
    @BindView(R.id.user_id_edt)
    public EditText user_id_edt;
    @BindView(R.id.login_btn)
    public Button login_btn;
    @BindView(R.id.welcome_txt)
    public TextView welcome_txt;
    @BindView(R.id.parent_lay)
    public RelativeLayout parent_lay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        String text = "<font color=#ffffff>Welcome to</font> <font color=#00B0F0>CY</font><font color=#F70573>O</font>";
        welcome_txt.setText(Html.fromHtml(text));
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (user_id_edt.getText().toString().trim().isEmpty()) {
                    GeneralFunction.showSnackBar(LoginActivity.this, parent_lay, "Please enter user id");
                } else if (mobile_no_edt.getText().toString().trim().isEmpty()) {
                    GeneralFunction.showSnackBar(LoginActivity.this, parent_lay, "Please enter mobile number");
                } else if (mobile_no_edt.getText().toString().length() < 10) {
                    GeneralFunction.showSnackBar(LoginActivity.this, parent_lay, "Incorrect user ID or phone number");
                } else {
                    if (GeneralFunction.isNetworkConnected(LoginActivity.this, parent_lay)) {
                        loginApi();
                    }
                }
            }
        });
    }

    private void loginApi() {

        String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("user_id", user_id_edt.getText().toString().trim());
        hashMap.put("mobile", mobile_no_edt.getText().toString().trim());
        hashMap.put("device_id", android_id);
        hashMap.put("device_type", "A");

        Log.e("login params", "" + hashMap);

        GeneralFunction.showProgress(LoginActivity.this);
        RestClient.getModalApiService().apiDoctorLogin(hashMap).enqueue(new Callback<DoctorLoginOutput>() {
            @Override
            public void onResponse(Call<DoctorLoginOutput> call, Response<DoctorLoginOutput> response) {
                GeneralFunction.dismissProgress();
                if (response.isSuccessful()) {
                    if (response.body().status == 1) {

                        Intent intent = new Intent(LoginActivity.this, VerifyActivity.class);
                        intent.putExtra("userId",String.valueOf(response.body().userDetail.userId));
                        intent.putExtra("mobile",String.valueOf(response.body().userDetail.phonenumber));
                        startActivity(intent);
                        finish();
                        Log.e("login res", "" + new Gson().toJson(response.body()));
                    } else {
                        Toast.makeText(LoginActivity.this, "" + response.body().message, Toast.LENGTH_SHORT).show();
                    }
                           /* if(view!=null) {
                                view.signupSuccess(response.body().getData());
                            }*/

                } else {
                    GeneralFunction.dismissProgress();
                        /*    if(response.code() == UnAuthorized)
                            {
                                if(view!=null) {
                                    view.sessionExpired();
                                }
                            }
                            else {
                                try {
                                    if(view!=null) {
                                        view.signupFailure(new JSONObject(response.errorBody().string()).getString("message"));
                                    }
                                } catch (JSONException | IOException e) {
                                    e.printStackTrace();
                                }
                            }*/
                }
            }

            @Override
            public void onFailure(Call<DoctorLoginOutput> call, Throwable t) {
                //  if(view!=null) {
                GeneralFunction.dismissProgress();
                //   view.signupFailure(t.getMessage());
                // }
            }

        });

    }
}
