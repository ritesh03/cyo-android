package com.cyo.app.view;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;


import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cyo.app.R;
import com.cyo.app.model.AddPatientOutput;
import com.cyo.app.model.DoctorLoginOutput;
import com.cyo.app.utils.Constant;
import com.cyo.app.utils.GeneralFunction;
import com.cyo.app.utils.Prefs;
import com.cyo.app.webservices.RestClient;
import com.google.gson.Gson;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class WriteFeedbackActivity extends AppCompatActivity {
    EditText feedBackTexs;
    RelativeLayout submit_review_btn, back_lay, parent_lay;
    DoctorLoginOutput doctorLoginOutput;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_feedback);
        doctorLoginOutput = Prefs.with(WriteFeedbackActivity.this).getObject(Constant.USER_DATA, DoctorLoginOutput.class);

        feedBackTexs = findViewById(R.id.feedBackTexs);
        submit_review_btn = findViewById(R.id.submit_review_btn);
        back_lay = findViewById(R.id.back_lay);
        parent_lay = findViewById(R.id.parent_lay);

        feedBackTexs.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        submit_review_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (GeneralFunction.isNetworkConnected(WriteFeedbackActivity.this,parent_lay)) {

if(feedBackTexs.getText().toString().equalsIgnoreCase(""))
{
    GeneralFunction.showSnackBar(WriteFeedbackActivity.this,parent_lay, "Please enter some text");
}
else
{
                        feedbackApi(feedBackTexs.getText().toString() );}

                } else {

                    GeneralFunction.showSnackBar(WriteFeedbackActivity.this,parent_lay, "No internet connection");

                }
            }
        });
        back_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private void feedbackApi(String msg ) {


        int version=android.os.Build.VERSION.SDK_INT;
        String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("user_id", String.valueOf(doctorLoginOutput.userDetail.userId));
        hashMap.put("message", msg);
        hashMap.put("device_detail", String.valueOf(version));



        Log.e("feedback params", "" + hashMap);

        GeneralFunction.showProgress(WriteFeedbackActivity.this);
        RestClient.getModalApiService().apiFeedback(hashMap).enqueue(new Callback<AddPatientOutput>() {
            @Override
            public void onResponse(Call<AddPatientOutput> call, Response<AddPatientOutput> response) {
                GeneralFunction.dismissProgress();
                if (response.isSuccessful()) {
                    Log.e("login res", "" + new Gson().toJson(response.body()));

                    if(response.body().status==1)
                    {
                        feedBackTexs.setText("");
                        Toast.makeText(WriteFeedbackActivity.this, "Feedback submitted successfully", Toast.LENGTH_SHORT).show();
                       /* Intent intent=new Intent(WriteFeedbackActivity.this,HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);*/
                        //finish();
                    }
                    else
                    {
                        GeneralFunction.showSnackBar(WriteFeedbackActivity.this,parent_lay,response.body().message);
                    }

                } else {


                }
            }

            @Override
            public void onFailure(Call<AddPatientOutput> call, Throwable t) {
                //  if(view!=null) {
                GeneralFunction.dismissProgress();
                //   view.signupFailure(t.getMessage());
                // }
            }

        });

    }

}
