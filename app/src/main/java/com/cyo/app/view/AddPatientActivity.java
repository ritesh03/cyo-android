package com.cyo.app.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.cyo.app.R;

import com.cyo.app.model.AddPatientOutput;
import com.cyo.app.model.BlockOutput;
import com.cyo.app.model.DoctorLoginOutput;
import com.cyo.app.model.GetPatientOutput;
import com.cyo.app.utils.Constant;
import com.cyo.app.utils.GeneralFunction;
import com.cyo.app.utils.Prefs;
import com.cyo.app.webservices.ApiService;
import com.cyo.app.webservices.RestClient;
import com.google.gson.Gson;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import adil.dev.lib.materialnumberpicker.dialog.GenderPickerDialog;
import adil.dev.lib.materialnumberpicker.dialog.NumberPickerDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


public class AddPatientActivity extends AppCompatActivity {
    @BindView(R.id.back_lay)
    public RelativeLayout back_lay;
    @BindView(R.id.title_txtview)
    public TextView title_txtview;
    @BindView(R.id.submit_add_btn)
    public Button submit_add_btn;
    @BindView(R.id.parent_lay)
    public RelativeLayout parent_lay;

    @BindView(R.id.select_gender)
    public TextView select_gender;
    @BindView(R.id.select_age)
    public TextView select_age;
    @BindView(R.id.patient_name_edt)
    public EditText patient_name_edt;
    @BindView(R.id.patient_lname_edt)
    public EditText patient_lname_edt;
    @BindView(R.id.patient_no_edt)
    public EditText patient_no_edt;
    @BindView(R.id.patient_email_edt)
    public EditText patient_email_edt;
    @BindView(R.id.profile_imggg)
    public CircleImageView profile_imgg;
    Uri resultUri;
    private static final int PERMISSION_REQUEST_CODE = 33;
    DoctorLoginOutput doctorLoginOutput;
    String age = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_patient);
        ButterKnife.bind(this);
        doctorLoginOutput = Prefs.with(AddPatientActivity.this).getObject(Constant.USER_DATA, DoctorLoginOutput.class);

        title_txtview.setText("Add Patient");
        back_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        profile_imgg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(AddPatientActivity.this);

            }
        });

        select_age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                NumberPickerDialog dialog = new NumberPickerDialog(AddPatientActivity.this, 1, 100, new NumberPickerDialog.NumberPickerCallBack() {
                    @Override
                    public void onSelectingValue(int value) {
                        // Toast.makeText(AddPatientActivity.this, "Selected "+String.valueOf(value), Toast.LENGTH_SHORT).show();

                        select_age.setText(String.valueOf(value) + " years");
                        age = String.valueOf(value);
                    }
                });
                dialog.show();
            }
        });
        submit_add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
GeneralFunction.hideKeyboard(AddPatientActivity.this);
                if(validationCheck())
                {
                if (resultUri != null) {
                    if(GeneralFunction.isNetworkConnected(AddPatientActivity.this,parent_lay))
                    {
                    addpatientWithImageApi();}
                } else {

                    if(GeneralFunction.isNetworkConnected(AddPatientActivity.this,parent_lay))
                    {
                    addPatientsApi();}
                }}
            }
        });
       /* select_gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GenderPickerDialog dialog=new GenderPickerDialog(AddPatientActivity.this);
                dialog.setOnSelectingGender(new GenderPickerDialog.OnGenderSelectListener() {
                    @Override
                    public void onSelectingGender(String value) {
                        Toast.makeText(AddPatientActivity.this, "Selected "+value, Toast.LENGTH_SHORT).show();
                        select_gender.setText(value);
                    }
                });
                dialog.show();
            }
        });*/
    }

    private boolean validationCheck() {


        String firstName = patient_name_edt.getText().toString().trim();
        String lastName = patient_lname_edt.getText().toString().trim();
        String age = select_age.getText().toString().trim();
        String phoneNo = patient_no_edt.getText().toString().trim();
        String email = patient_email_edt.getText().toString().trim();

        if (firstName.isEmpty()) {
            GeneralFunction.showSnackBar(AddPatientActivity.this, parent_lay, "Please enter first name");

            return false;
        } /*else if (!TextUtils.isCharacterOrSpaceOnly(firstName)) {
            GeneralFunction.showSnackBar(getActivity(), binding.parent, getString(R.string.first_name_should_not_contain_number));
            binding.etFirstName.requestFocus();
            return false;
        }*/ else if (lastName.isEmpty()) {
            GeneralFunction.showSnackBar(AddPatientActivity.this, parent_lay, "Please enter last name");
            return false;
        } /*else if (!TextUtils.isCharacterOrSpaceOnly(lastName)) {
            GeneralFunction.showSnackBar(getActivity(), binding.parent, getString(R.string.last_name_should_not_contain_number));
            binding.etLastName.requestFocus();
            return false;
        }*/ else if (age.isEmpty()) {
            GeneralFunction.showSnackBar(AddPatientActivity.this, parent_lay, "Please enter age");

            return false;
        }
        else if (phoneNo.isEmpty()) {
            GeneralFunction.showSnackBar(AddPatientActivity.this, parent_lay, "Please enter contact number");
            return false;
        } else if (phoneNo.length()<10) {
            GeneralFunction.showSnackBar(AddPatientActivity.this, parent_lay, "Please enter valid contact number");
            return false;
        }

        else if (!email.isEmpty()&&!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            GeneralFunction.showSnackBar(AddPatientActivity.this, parent_lay, "Please enter valid email");

            return false;
        }




        return true;
    }

    private void addPatientsApi() {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("user_id", String.valueOf(doctorLoginOutput.userDetail.userId));
        hashMap.put("firstname", patient_name_edt.getText().toString().trim());
        hashMap.put("lastname", patient_lname_edt.getText().toString().trim());
        hashMap.put("gender", "1");
        hashMap.put("email", patient_email_edt.getText().toString().trim());
        hashMap.put("mobile", patient_no_edt.getText().toString().trim());
        hashMap.put("age", age);


        Log.e("add patient params", "" + hashMap);

        GeneralFunction.showProgress(AddPatientActivity.this);
        RestClient.getModalApiService().apiAddPatient(hashMap).enqueue(new Callback<AddPatientOutput>() {
            @Override
            public void onResponse(Call<AddPatientOutput> call, Response<AddPatientOutput> response) {
                GeneralFunction.dismissProgress();
                if (response.isSuccessful()) {
                    Log.e("login res", "" + new Gson().toJson(response.body()));

                    if(response.body().status==1)
                    {
                        Toast.makeText(AddPatientActivity.this, "Patient added successfully", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
GeneralFunction.showSnackBar(AddPatientActivity.this,parent_lay,response.body().message);
                    }

                } else {


                }
            }

            @Override
            public void onFailure(Call<AddPatientOutput> call, Throwable t) {
                //  if(view!=null) {
                GeneralFunction.dismissProgress();
                //   view.signupFailure(t.getMessage());
                // }
            }

        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        blockStatusApi();
    }

    private void blockStatusApi() {

        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("user_id", String.valueOf(doctorLoginOutput.userDetail.userId));



        Log.e("get patient params",""+hashMap);

        //     GeneralFunction.showProgress(HomeActivity.this);
        RestClient.getModalApiService().apiBlockStatus(hashMap).enqueue(new Callback<BlockOutput>() {
            @Override
            public void onResponse(Call<BlockOutput> call, Response<BlockOutput> response) {
                //   GeneralFunction.dismissProgress();
                if (response.isSuccessful()) {
                    Log.e("login res",""+new Gson().toJson(response.body()));


                    if(response.body().doctorStatus.equalsIgnoreCase("1"))
                    {

                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(AddPatientActivity.this);


                        //Uncomment the below code to Set the message and title from the strings.xml file
                        builder.setMessage("Your account is blocked by admin!") .setTitle("Block");

                        //Setting message manually and performing action on button click
                        builder.setMessage("Your account is blocked by admin!")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Prefs.get().removeAll();
                                        Intent intent=new Intent(AddPatientActivity.this,LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                });
                                       /* .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                //  Action for 'NO' Button
                                                dialog.cancel();
                                                Toast.makeText(getApplicationContext(),"you choose no action for alertbox",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        });*/
                        //Creating dialog box
                        android.app.AlertDialog alert = builder.create();
                        //Setting the title manually
                        alert.setTitle("Block");
                        alert.show();

                    }
                    else if(response.body().doctorStatus.equalsIgnoreCase("2"))
                    {}
                    String android_id = Settings.Secure.getString(getContentResolver(),
                            Settings.Secure.ANDROID_ID);
                    if(response.body().device_id.equalsIgnoreCase(android_id))
                    {}
                    else
                    {
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(AddPatientActivity.this);


                        //Uncomment the below code to Set the message and title from the strings.xml file
                        builder.setMessage("Your account is refreshed by the admin. Please login again!") .setTitle("Alert!");

                        //Setting message manually and performing action on button click
                        builder.setMessage("Your account is refreshed by the admin. Please login again!")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Prefs.get().removeAll();
                                        Intent intent=new Intent(AddPatientActivity.this,LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                });
                                       /* .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                //  Action for 'NO' Button
                                                dialog.cancel();
                                                Toast.makeText(getApplicationContext(),"you choose no action for alertbox",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        });*/
                        //Creating dialog box
                        android.app.AlertDialog alert = builder.create();
                        //Setting the title manually
                        alert.setTitle("Alert!");
                        alert.show();
                    }


                }
                else {



                }
            }

            @Override
            public void onFailure(Call<BlockOutput> call, Throwable t) {

            }

        });

    }
    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{CAMERA, WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);

    }

    private void addpatientWithImageApi() {
        GeneralFunction.showProgress(AddPatientActivity.this);

        MultipartBody.Part imageUser = null;
        if (resultUri != null) {
            final RequestBody reqFile;
            try {
                reqFile = RequestBody.create(MediaType.parse("image/*"), new File(new URI(resultUri.toString())));
                imageUser = MultipartBody.Part.createFormData("image", new File(new URI(resultUri.toString())).getName(), reqFile);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

        }
        RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(doctorLoginOutput.userDetail.userId));
        RequestBody first_name = RequestBody.create(MediaType.parse("text/plain"), patient_name_edt.getText().toString().trim());
        RequestBody last_name = RequestBody.create(MediaType.parse("text/plain"), patient_lname_edt.getText().toString().trim());
        RequestBody gender = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), patient_email_edt.getText().toString().trim());
        RequestBody mobile = RequestBody.create(MediaType.parse("text/plain"), patient_no_edt.getText().toString().trim());
        RequestBody patient_age = RequestBody.create(MediaType.parse("text/plain"), age);

        Gson gson = new Gson();
        Log.e("resultUri params", resultUri + "");
        Log.e("userId params", String.valueOf(doctorLoginOutput.userDetail.userId) + "");
        Log.e("userId params", patient_name_edt.getText().toString().trim() + "");
        Log.e("userId params", patient_name_edt.getText().toString() + "");
        Log.e("email params", patient_email_edt.getText().toString().trim() + "");
        Log.e("mobile params", patient_no_edt.getText().toString().trim() + "");
        Log.e("age params", age + "");

        RestClient.getModalApiService().apiAddPatientWithImage(imageUser, user_id, first_name, last_name, gender, email, mobile, patient_age).enqueue(new Callback<AddPatientOutput>() {
            @Override
            public void onResponse(Call<AddPatientOutput> call, Response<AddPatientOutput> response) {


                GeneralFunction.dismissProgress();

                if(response.body().status==1)
                {
                    Toast.makeText(AddPatientActivity.this, "Patient added successfully", Toast.LENGTH_SHORT).show();
                    finish();
                }
                else
                {
                    GeneralFunction.showSnackBar(AddPatientActivity.this,parent_lay,response.body().message);
                }

                Log.e("res", "" + new Gson().toJson(response.body()));
            }

            @Override
            public void onFailure(Call<AddPatientOutput> call, Throwable t) {
                //  commonFunction.hideProgressDialog();
                GeneralFunction.dismissProgress();
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean storageAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;


                    if (storageAccepted && cameraAccepted) {
                       /* Intent intent = new Intent(AddPatientActivity.this, ImageSelectActivity.class);
                        intent.putExtra(ImageSelectActivity.FLAG_COMPRESS, false);//default is true
                        intent.putExtra(ImageSelectActivity.FLAG_CAMERA, true);//default is true
                        intent.putExtra(ImageSelectActivity.FLAG_GALLERY, true);//default is true
                        startActivityForResult(intent, 1213);*/
                        //  camera_icon.performClick();
                    }
                    //Snackbar.make(view, "Permission Granted, Now you can access location data and camera.", Snackbar.LENGTH_LONG).show();
                    else {

                        //  Snackbar.make(view, "Permission Denied, You cannot access location data and camera.", Snackbar.LENGTH_LONG).show();

                    }

                }
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1213 && resultCode == Activity.RESULT_OK) {
          /*  String filePath = data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH);
            Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
            profile_img.setImageBitmap(selectedImage);*/
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                //  String filePath = data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH);
                resultUri = result.getUri();
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                profile_imgg.setImageBitmap(bitmap);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

}
