package com.cyo.app.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.cyo.app.R;
import com.cyo.app.model.DoctorLoginOutput;
import com.cyo.app.model.SharePdfOutput;
import com.cyo.app.utils.Constant;
import com.cyo.app.utils.GeneralFunction;
import com.cyo.app.utils.Prefs;
import com.cyo.app.webservices.RestClient;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cyo.app.view.ScreenImagesActivity.image_ids;

public class Webview_activity extends AppCompatActivity {

    WebView wv1;
    String pregnent_txt = "no";
    String smoker_txt = "no";
    String verbal_txt = "";
    String ae_txt = "";
    String awe_txt = "";
    String pun_txt = "";
    String mos_txt = "";
    String vulva_txt = "";
    String imp_txt = "";
    String tdt_txt = "";
    String pm_txt = "no";
    String nameJs, phoneJs, emaill, emailJs, txtNumberJs, genderJs, ptnJs, edcJs, pnoJs, pyesJs, abJs, birth_edcJs, impre_otherJs, lug_otherJs, papa_colJs, smoker_historyJs, ds_contactJs, ds_nameJs, dr_emailJs, dr_nameJs, test_noteJs, test_resultJs;
    String name, phone, pyes, email, txtNumber, gender, ptn, edc, pno, ab, birth_edc, impre_other, lug_other, papa_col, smoker_history, ds_contact, ds_name, dr_email, dr_name, test_note, test_result;
    TextView title_txt, upload_sign_txt, date_txt;
    RelativeLayout back_lay;
    ImageView upload_sign_img;
    RelativeLayout select_btn;
    String url;
    String test_oblig, uptake_select, margin_select, Vessels_select, Lesion_select, iodine_select, post_yes, post_no, mosaic_coarse, smoker_yes, smoker_no, shiny, dull, verbal_permit, flat, raised, awe_indis, awe_sharp, awe_boder, pun_fine, pun_coarse, mosaic_fine, number,
            peeling, high_grade, low_grade, insp_normal, colposcoped, vul_insp, test_pap, test_dnd, test_urt;
    private static final String SEPARATOR = ",";
    DoctorLoginOutput doctorLoginOutput;
    ArrayList<String> results;
    private int mYear, mMonth, mDay, mHour, mMinute;

    ArrayList<CheckBox> checkboxes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_webview_activity);
        doctorLoginOutput = Prefs.with(Webview_activity.this).getObject(Constant.USER_DATA, DoctorLoginOutput.class);
        pregnent_txt = "";
        pm_txt = "";
        verbal_txt = "";
        awe_txt = "";
        imp_txt = "";
        tdt_txt = "";
        vulva_txt = "";
        mos_txt = "";
        pun_txt = "";
        ae_txt = "";
        smoker_txt = "";
        wv1 = findViewById(R.id.calculator_web_view);
        title_txt = findViewById(R.id.title_txt);
        title_txt.setText("");
        results = new ArrayList<>();
        back_lay = findViewById(R.id.back_lay);
        back_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = "";

                finish();
            }
        });
        select_btn = findViewById(R.id.select_btn);

        select_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //asdfa = "javascript:document.getElementById('pyes').checked" ;
                nameJs = "javascript:document.getElementById('name').value";
                phoneJs = "javascript:document.getElementById('phone').value";
                emailJs = "javascript:document.getElementById('email').value";
                genderJs = "javascript:document.getElementById('gender').value";
                ptnJs = "javascript:document.getElementById('ptn').value";
                txtNumberJs = "javascript:document.getElementById('txtNumber').value";
                abJs = "javascript:document.getElementById('ab').value";

                edcJs = "javascript:document.getElementById('edc').value";
                birth_edcJs = "javascript:document.getElementById('birth_edc').value";
                smoker_historyJs = "javascript:document.getElementById('smoker_history').value";
                wv1.evaluateJavascript(smoker_historyJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        smoker_history = s;
                    }
                });
                papa_colJs = "javascript:document.getElementById('papa_col').value";
                wv1.evaluateJavascript(papa_colJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        papa_col = s;
                    }
                });
                lug_otherJs = "javascript:document.getElementById('lug_other').value";
                wv1.evaluateJavascript(lug_otherJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        lug_other = s;
                    }
                });
                impre_otherJs = "javascript:document.getElementById('impre_other').value";
                wv1.evaluateJavascript(impre_otherJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        impre_other = s;
                    }
                });
                test_resultJs = "javascript:document.getElementById('test_result').value";
                wv1.evaluateJavascript(test_resultJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        test_result = s;
                    }
                });
                test_noteJs = "javascript:document.getElementById('test_note').value";
                wv1.evaluateJavascript(test_noteJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        test_note = s;
                    }
                });
                dr_nameJs = "javascript:document.getElementById('dr_name').value";
                wv1.evaluateJavascript(dr_nameJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        dr_name = s;
                    }
                });
                dr_emailJs = "javascript:document.getElementById('dr_email').value";
                wv1.evaluateJavascript(dr_emailJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        dr_email = s;
                    }
                });


                final String uptake_selectJs = "javascript:document.getElementById('uptake_select').value";
                wv1.evaluateJavascript(uptake_selectJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        uptake_select = s;
                    }
                });


                final String margin_selectJs = "javascript:document.getElementById('margin_select').value";
                wv1.evaluateJavascript(margin_selectJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        margin_select = s;
                    }
                });
                final String Vessels_selectJs = "javascript:document.getElementById('Vessels_select').value";
                wv1.evaluateJavascript(Vessels_selectJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        Vessels_select = s;
                    }
                });
                final String Lesion_selectJs = "javascript:document.getElementById('Lesion_select').value";
                wv1.evaluateJavascript(Lesion_selectJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        Lesion_select = s;
                    }
                });
                final String iodine_selectJs = "javascript:document.getElementById('iodine_select').value";
                wv1.evaluateJavascript(iodine_selectJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        iodine_select = s;
                    }
                });
                pyesJs = "javascript:document.getElementById('pyes').checked";
                wv1.evaluateJavascript(pyesJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        pyes = s;
                        if (s.equalsIgnoreCase("true")) {
                            pregnent_txt = "yes";
                        }
                    }
                });
                pnoJs = "javascript:document.getElementById('pno').checked";
                wv1.evaluateJavascript(pnoJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        pno = s;
                        if (s.equalsIgnoreCase("true")) {
                            pregnent_txt = "yes";
                        }
                    }
                });
                final String post_yesJs = "javascript:document.getElementById('post_yes').checked";
                wv1.evaluateJavascript(post_yesJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        post_yes = s;
                        if (s.equalsIgnoreCase("true")) {
                            pm_txt = "yes";
                        }

                    }
                });
                final String post_noJs = "javascript:document.getElementById('post_no').checked";
                wv1.evaluateJavascript(post_noJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        post_no = s;
                        if (s.equalsIgnoreCase("true")) {
                            pm_txt = "yes";
                        }
                    }
                });
                final String smoker_yesJs = "javascript:document.getElementById('smoker_yes').checked";
                wv1.evaluateJavascript(smoker_yesJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        smoker_yes = s;

                        if (s.equalsIgnoreCase("true")) {
                            smoker_txt = "yes";
                        }
                    }
                });


                final String smoker_noJs = "javascript:document.getElementById('smoker_no').checked";
                wv1.evaluateJavascript(smoker_noJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        smoker_no = s;
                        if (s.equalsIgnoreCase("true")) {
                            smoker_txt = "yes";
                        }
                    }
                });
                final String shinyJs = "javascript:document.getElementById('shiny').checked";
                wv1.evaluateJavascript(shinyJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        shiny = s;
                        if (s.equalsIgnoreCase("true")) {
                            ae_txt = "Shiny";
                        }

                    }
                });
                final String dullJs = "javascript:document.getElementById('dull').checked";
                wv1.evaluateJavascript(dullJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        dull = s;
                        if (s.equalsIgnoreCase("true")) {
                            ae_txt = "Dull";
                        }
                    }
                });
                final String verbal_permitJs = "javascript:document.getElementById('verbal_permit').checked";
                wv1.evaluateJavascript(verbal_permitJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        verbal_permit = s;
                        if (s.equalsIgnoreCase("true")) {
                            verbal_txt = "Verbal Permit obtained from patient";
                        } else {
                            verbal_txt = "";
                        }

                    }
                });
                final String peelingJs = "javascript:document.getElementById('peeling').checked";
                wv1.evaluateJavascript(peelingJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        peeling = s;
                        if (s.equalsIgnoreCase("true")) {
                            ae_txt = "Peeling";
                        }
                    }
                });
                final String flatJs = "javascript:document.getElementById('flat').checked";
                wv1.evaluateJavascript(flatJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        flat = s;
                        if (s.equalsIgnoreCase("true")) {
                            ae_txt = "Flat";
                        }
                    }
                });
                final String raisedJs = "javascript:document.getElementById('raised').checked";
                wv1.evaluateJavascript(raisedJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        raised = s;
                        if (s.equalsIgnoreCase("true")) {
                            ae_txt = "Raised";
                        }
                    }
                });
                final String awe_indisJs = "javascript:document.getElementById('awe_indis').checked";
                wv1.evaluateJavascript(awe_indisJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        awe_indis = s;
                        if (s.equalsIgnoreCase("true")) {
                            awe_txt = "Indistinct";
                        }

                    }
                });


                final String awe_sharpJs = "javascript:document.getElementById('awe_sharp').checked";
                wv1.evaluateJavascript(awe_sharpJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        awe_sharp = s;
                        if (s.equalsIgnoreCase("true")) {
                            awe_txt = "Sharp";
                        }
                    }
                });
                final String awe_boderJs = "javascript:document.getElementById('awe_boder').checked";
                wv1.evaluateJavascript(awe_boderJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        awe_boder = s;
                        if (s.equalsIgnoreCase("true")) {
                            awe_txt = "Border w/I border";
                        }
                    }
                });
                final String pun_fineJs = "javascript:document.getElementById('pun_fine').checked";
                wv1.evaluateJavascript(pun_fineJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        pun_fine = s;

                        if (s.equalsIgnoreCase("true")) {
                            pun_txt = "Fine";
                        }
                    }
                });
                final String pun_coarseJs = "javascript:document.getElementById('pun_coarse').checked";
                wv1.evaluateJavascript(pun_coarseJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        pun_coarse = s;
                        if (s.equalsIgnoreCase("true")) {
                            pun_txt = "Coarse";
                        }
                    }
                });
                final String mosaic_fineJs = "javascript:document.getElementById('mosaic_fine').checked";
                wv1.evaluateJavascript(mosaic_fineJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        mosaic_fine = s;
                        if (s.equalsIgnoreCase("true")) {
                            mos_txt = "Fine";
                        }

                    }
                });
                final String mosaic_coarseJs = "javascript:document.getElementById('mosaic_coarse').checked";
                wv1.evaluateJavascript(mosaic_coarseJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        mosaic_coarse = s;
                        if (s.equalsIgnoreCase("true")) {
                            mos_txt = "Coarse";
                        }
                    }
                });
                final String vul_inspJs = "javascript:document.getElementById('vul_insp').checked";
                wv1.evaluateJavascript(vul_inspJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        vul_insp = s;
                        if (s.equalsIgnoreCase("true")) {
                            vulva_txt = "Inspected";
                        }

                    }
                });
                final String colposcopedJs = "javascript:document.getElementById('colposcoped').checked";
                wv1.evaluateJavascript(colposcopedJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        colposcoped = s;
                        if (s.equalsIgnoreCase("true")) {
                            vulva_txt = "Colposcoped";
                        }
                    }
                });
                String insp_normalJs = "javascript:document.getElementById('insp_normal').checked";
                wv1.evaluateJavascript(insp_normalJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        insp_normal = s;
                        if (s.equalsIgnoreCase("true")) {
                            imp_txt = "Normal";
                        }
                    }
                });
                final String low_gradeJs = "javascript:document.getElementById('low_grade').checked";
                wv1.evaluateJavascript(low_gradeJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        low_grade = s;
                        if (s.equalsIgnoreCase("true")) {
                            imp_txt = "Low Grade";
                        }
                    }
                });
                final String high_gradeJs = "javascript:document.getElementById('high_grade').checked";
                wv1.evaluateJavascript(high_gradeJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        high_grade = s;
                        if (s.equalsIgnoreCase("true")) {
                            imp_txt = "High Grade";
                        }
                    }
                });
                final String test_papJs = "javascript:document.getElementById('test_pap').checked";
                wv1.evaluateJavascript(test_papJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        test_pap = s;
                        if (s.equalsIgnoreCase("true")) {
                            tdt_txt = "Pap";
                        }

                    }
                });
                final String test_dndJs = "javascript:document.getElementById('test_dnd').checked";
                wv1.evaluateJavascript(test_dndJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        test_dnd = s;
                        if (s.equalsIgnoreCase("true")) {
                            tdt_txt = "HPV-DNA";
                        }
                    }
                });
                final String test_obligJS = "javascript:document.getElementById('test_oblig').checked";
                wv1.evaluateJavascript(test_obligJS, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        test_oblig = s;
                        if (s.equalsIgnoreCase("true")) {
                            tdt_txt = "Oblig.";
                        }
                    }
                });
                final String test_urtJs = "javascript:document.getElementById('test_urt').checked";
                wv1.evaluateJavascript(test_urtJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        test_urt = s;
                        if (s.equalsIgnoreCase("true")) {
                            tdt_txt = "UPT";
                        }

                    }
                });

                wv1.evaluateJavascript(nameJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        name = s;
                    }
                });

                wv1.evaluateJavascript(phoneJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        phone = s;
                    }
                });

                wv1.evaluateJavascript(emailJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        emaill = s;
                    }
                });

                wv1.evaluateJavascript(genderJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        gender = s;
                    }
                });

                wv1.evaluateJavascript(ptnJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        ptn = s;
                    }
                });

                wv1.evaluateJavascript(txtNumberJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        txtNumber = s;
                    }
                });

                wv1.evaluateJavascript(abJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        ab = s;
                    }
                });

                wv1.evaluateJavascript(edcJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        edc = s;
                    }
                });

                wv1.evaluateJavascript(birth_edcJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.e("ddd", "--" + s);
                        birth_edc = s;
                    }
                });


                sharePdfApi();

           /*   Log.e("data",    getElementsById("name phone email txtNumber gender ptn ab pyes pno edc birth_edc smoker_history papa_col lug_other impre_other test_result test_note dr_name dr_email ds_name ds_contact")
                +"");*/

//select_btn.performClick();

/*
                wv1.evaluateJavascript(nameJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        name=s;
                    }
                });
                wv1.evaluateJavascript(phoneJs, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        phone=s;
                    }
                });*/
                //Log.e("name",name+phone);
            }
        });

        wv1.setWebViewClient(new MyBrowser(Webview_activity.this));
        url = "http://119.81.1.69/~callmyma/cyo/show_pdf";
        //ds_nameJs = javascript:document.getElementById('ds_name').value='android';
        // ds_nameJs ="javascript:document.getElementById('ds_name').value='android'";



        phoneJs = "javascript:document.getElementById('phone').value='"+getIntent().getStringExtra("mobile")+"'";
        emailJs = "javascript:document.getElementById('email').value='"+getIntent().getStringExtra("patient_email")+"'";
        genderJs = "javascript:document.getElementById('gender').value='"+"F"+"'";
        nameJs = "javascript: document.getElementById('name').value='"+getIntent().getStringExtra("fname")+" "+getIntent().getStringExtra("lname")+"'";
        txtNumberJs = "javascript:document.getElementById('txtNumber').value='"+getIntent().getStringExtra("age")+"'";

        ds_nameJs = "javascript: document.getElementById('ds_name').value='"+doctorLoginOutput.userDetail.firstname+" "+doctorLoginOutput.userDetail.lastname+"'";

       // ds_contactJs = "javascript:document.getElementById('ds_contact').value";
       // ds_contactJs = "javascript: document.getElementById('ds_name').value='"+doctorLoginOutput.userDetail.phonenumber+"'";
        ds_contactJs = "javascript: document.getElementById('ds_contact').value='"+doctorLoginOutput.userDetail.phonenumber+"'";

        wv1.getSettings().setLoadsImagesAutomatically(true);
        wv1.getSettings().setJavaScriptEnabled(true);
        wv1.getSettings().setDomStorageEnabled(true);
        Object jsi = new Object() {
            @JavascriptInterface
            public void reportCheckboxState(String name, boolean isChecked) {

            }
        };
        wv1.addJavascriptInterface(jsi, "injection");
        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        wv1.loadUrl(url);
    }

    public ArrayList<String> getElementsById(String ids) {
        String[] idList = ids.split(" ");
        String item = "";
        //  results=new ArrayList<>();
        for (int i = 0; i < idList.length; i++) {
            item = "javascript:document.getElementById('" + idList[i] + "').value";
          /*  if (!item.equalsIgnoreCase("")) {
                results.add(item);
            }*/

            wv1.evaluateJavascript(item, new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String s) {
                    results.add(s);
                }
            });

        }
        Log.e("list", results + "");

        return results;
    }

    public void getElementsByIdDummy(WebView view, String ids) {
        String[] idList = ids.split(" ");
        String item = "";
        //  results=new ArrayList<>();
        for (int i = 0; i < idList.length; i++) {
            item = "javascript:document.getElementById('" + idList[i] + "').value";
          /*  if (!item.equalsIgnoreCase("")) {
                results.add(item);
            }*/

            view.evaluateJavascript(item, new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String s) {
                    // results.add(s);
                }
            });

        }
        // Log.e("list",results+"");

        //return results;
    }

    public String getMyDateFormat(String date, String currentFormat, String format) {
        String finalDate;
        SimpleDateFormat curFormater = new SimpleDateFormat(currentFormat, Locale.ENGLISH);
        Date dateObj = null;
        try {
            dateObj = curFormater.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat postFormater;
        postFormater = new SimpleDateFormat(format, Locale.ENGLISH);
        if (dateObj != null)
            finalDate = postFormater.format(dateObj);
        else
            finalDate = "";
        return finalDate;
    }

    @Override
    protected void onResume() {
        super.onResume();
        // getWebViewValues();

    }

    private void getWebViewValues() {
        wv1.evaluateJavascript(nameJs, new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String s) {
                name = s;
            }
        });
        wv1.evaluateJavascript(phoneJs, new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String s) {
                phone = s;
            }
        });


    }


    private class MyBrowser extends WebViewClient {
        Context context;

        public MyBrowser(Context context) {
            this.context = context;
            GeneralFunction.showProgress(context);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            view.evaluateJavascript(ds_nameJs, new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String s) {
                }
            });
            view.evaluateJavascript(ds_contactJs, new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String s) {
                }
            }); view.evaluateJavascript(txtNumberJs, new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String s) {
                }
            }); view.evaluateJavascript(nameJs, new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String s) {
                }
            }); view.evaluateJavascript(genderJs, new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String s) {
                }
            }); view.evaluateJavascript(emailJs, new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String s) {
                }
            }); view.evaluateJavascript(phoneJs, new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String s) {
                }
            });



            phoneJs = "javascript:document.getElementById('phone').value='"+getIntent().getStringExtra("mobile")+"'";
            emailJs = "javascript:document.getElementById('email').value='"+getIntent().getStringExtra("patient_email")+"'";
            genderJs = "javascript:document.getElementById('gender').value='"+getIntent().getStringExtra("gender")+"'";
            nameJs = "javascript: document.getElementById('name').value='"+getIntent().getStringExtra("fname")+" "+getIntent().getStringExtra("lname")+"'";
            txtNumberJs = "javascript:document.getElementById('txtNumber').value='"+getIntent().getStringExtra("age")+"'";





            //   getElementsById("name phone email txtNumber gender ptn ab pyes pno edc birth_edc smoker_history papa_col lug_other impre_other test_result test_note dr_name dr_email ds_name ds_contact");
            getElementsByIdDummy(view, "name phone email txtNumber gender ptn ab pyes pno edc birth_edc smoker_history papa_col lug_other impre_other test_result test_note dr_name dr_email ds_name ds_contact");
            GeneralFunction.dismissProgress();
        }
    }

    private void sharePdfApi() {
        StringBuilder csvBuilder = new StringBuilder();
        for (String city : image_ids) {
            csvBuilder.append(city);
            csvBuilder.append(SEPARATOR);
        }
        String csv = csvBuilder.toString();
        System.out.println(csv);
        //OUTPUT: Milan,London,New York,San Francisco,
        //Remove last comma
        csv = csv.substring(0, csv.length() - SEPARATOR.length());
        Log.e("list", "" + csv);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("user_id", String.valueOf(doctorLoginOutput.userDetail.userId));
        hashMap.put("user_type", "2");
        hashMap.put("email", getIntent().getStringExtra("email"));
        hashMap.put("image_ids", csv);
        hashMap.put("patient_name", name);

        hashMap.put("patient_phone", phone);
        hashMap.put("patient_email_id", emaill);
        hashMap.put("patient_age", txtNumber);
        hashMap.put("g", gender);
        hashMap.put("p", ptn);
        hashMap.put("ab", ab);
        hashMap.put("pregnent", pregnent_txt);
        hashMap.put("edc", edc);
        hashMap.put("lmp", birth_edc);
        hashMap.put("post_menopausal", pm_txt);
        hashMap.put("smoker", smoker_txt);
        hashMap.put("smoking_history", smoker_history);
        hashMap.put("significant_history", papa_col);
        hashMap.put("verbal_permit_patient", verbal_txt);
        hashMap.put("acetowhite_epithelium", ae_txt);
        hashMap.put("awe_border", awe_txt);
        hashMap.put("punctation", pun_txt);
        hashMap.put("mosaic", mos_txt);
        hashMap.put("lugol_other", lug_other);
        hashMap.put("vulva", vulva_txt);
        hashMap.put("impression", imp_txt);
        hashMap.put("other", impre_other);
        hashMap.put("uptake_acetic_acid", uptake_select);
        hashMap.put("margins_surface", margin_select);
        hashMap.put("vessels", Vessels_select);
        hashMap.put("lesion_size", Lesion_select);
        hashMap.put("iodine_staining", iodine_select);
        hashMap.put("tests_done_today", tdt_txt);
        hashMap.put("results", test_result);
        hashMap.put("note", test_note);
        hashMap.put("doctor_receiver_name", dr_name);
        hashMap.put("doctor_receiver_emailid", dr_email);
        hashMap.put("doctor_sender_name", doctorLoginOutput.userDetail.firstname + " " + doctorLoginOutput.userDetail.lastname);
        hashMap.put("doctor_sender_contact_detail", doctorLoginOutput.userDetail.phonenumber);


        Log.e("share patient params", "" + hashMap);

        GeneralFunction.showProgress(Webview_activity.this);
        RestClient.getModalApiService().apiSharePdf(hashMap).enqueue(new Callback<SharePdfOutput>() {
            @Override
            public void onResponse(Call<SharePdfOutput> call, Response<SharePdfOutput> response) {
                GeneralFunction.dismissProgress();
                if (response.isSuccessful()) {
                    Log.e("share res", "" + new Gson().toJson(response.body()));

                    if (response.body().status.equalsIgnoreCase("1")) {
                        Intent intent = new Intent(Webview_activity.this, PdfViewActivity.class);
                        intent.putExtra("url", response.body().pdfPath);
                        intent.putExtra("email", getIntent().getStringExtra("email"));

                        intent.putExtra("fname", getIntent().getStringExtra("fname"));
                        intent.putExtra("lname", getIntent().getStringExtra("lname"));
                        intent.putExtra("dname", doctorLoginOutput.userDetail.firstname+" "+doctorLoginOutput.userDetail.lastname);

                        intent.putExtra("pdf_name", response.body().pdfName);
                        intent.putExtra("user_type", "2");
                        startActivity(intent);
                        finish();
                    } else {
                        //GeneralFunction.showSnackBar(Webview_activity.this,parent_lay,response.body().message);
                    }

                } else {


                }
            }

            @Override
            public void onFailure(Call<SharePdfOutput> call, Throwable t) {
                //  if(view!=null) {
                GeneralFunction.dismissProgress();
                //   view.signupFailure(t.getMessage());
                // }
            }

        });

    }

}
