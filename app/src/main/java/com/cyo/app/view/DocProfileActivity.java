package com.cyo.app.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.cyo.app.R;
import com.cyo.app.model.AddPatientOutput;
import com.cyo.app.model.BlockOutput;
import com.cyo.app.model.DoctorLoginOutput;
import com.cyo.app.utils.Constant;
import com.cyo.app.utils.GeneralFunction;
import com.cyo.app.utils.Prefs;
import com.cyo.app.webservices.RestClient;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DocProfileActivity extends AppCompatActivity {
    @BindView(R.id.back_lay)
    public RelativeLayout back_lay;
    @BindView(R.id.parent_lay)
    public RelativeLayout parent_lay;
    @BindView(R.id.title_txtview)
    public TextView title_txtview;
    @BindView(R.id.submit_btn)
    public TextView submit_btn;
    @BindView(R.id.update_btn)
    public TextView update_btn;
    @BindView(R.id.dr_name)
    public TextView dr_name;
    @BindView(R.id.dr_user_id)
    public TextView dr_user_id;
@BindView(R.id.dr_certificate_number)
    public TextView dr_certificate_number;
@BindView(R.id.dr_speciality)
    public TextView dr_speciality;
@BindView(R.id.dr_city)
    public TextView dr_city;
@BindView(R.id.dr_email)
    public EditText dr_email;
@BindView(R.id.dr_number)
    public EditText dr_number;
@BindView(R.id.dr_edu_number)
    public EditText dr_address;
@BindView(R.id.profile_img)
    public CircleImageView profile_img;
    Uri resultUri;

    @BindView(R.id.select_txtview)
    public TextView select_txtview;
    DoctorLoginOutput doctorLoginOutput;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doc_profile);
        doctorLoginOutput = Prefs.with(DocProfileActivity.this).getObject(Constant.USER_DATA, DoctorLoginOutput.class);

        ButterKnife.bind(this);
        title_txtview.setText("View Profile");
        select_txtview.setVisibility(View.VISIBLE);

        select_txtview.setText("Edit");
        dr_number.setText(doctorLoginOutput.userDetail.phonenumber);
        dr_email.setText(doctorLoginOutput.userDetail.email);
        dr_city.setText(doctorLoginOutput.userDetail.city);
        dr_speciality.setText(doctorLoginOutput.userDetail.specialization);
        dr_certificate_number.setText(doctorLoginOutput.userDetail.certificationNumber);
        dr_user_id.setText(doctorLoginOutput.userDetail.userId.toString());
        dr_address.setText(doctorLoginOutput.userDetail.address);
        dr_name.setText("Dr. "+doctorLoginOutput.userDetail.firstname+" "+doctorLoginOutput.userDetail.lastname);
        Glide.with(DocProfileActivity.this).load(doctorLoginOutput.userDetail.image).placeholder(R.drawable.placeholder).into(profile_img);

        back_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        profile_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(select_txtview.getVisibility()==View.VISIBLE)
                {}
                else {
                    CropImage.activity()
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .start(DocProfileActivity.this);
                }
            }
        });
        select_txtview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                select_txtview.setVisibility(View.GONE);
                dr_number.setEnabled(true);
                dr_email.setEnabled(true);
                dr_address.setEnabled(true);
                profile_img.setEnabled(true);
                update_btn.setVisibility(View.VISIBLE);
            }
        });


        update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GeneralFunction.hideKeyboard(DocProfileActivity.this);
                if(validationCheck())
                {
                    if (resultUri != null) {
                        if(GeneralFunction.isNetworkConnected(DocProfileActivity.this,parent_lay))
                        {
                            editDoctorWithImageApi();
                        }
                    } else {

                        if(GeneralFunction.isNetworkConnected(DocProfileActivity.this,parent_lay))
                        {
                            updateApi();}
                    }


                }

            }
        });


        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GeneralFunction.hideKeyboard(DocProfileActivity.this);
                if(validationCheck())
                {
                    if (resultUri != null) {
                        if(GeneralFunction.isNetworkConnected(DocProfileActivity.this,parent_lay))
                        {
                            editDoctorWithImageApi();
                        }
                    } else {

                        if(GeneralFunction.isNetworkConnected(DocProfileActivity.this,parent_lay))
                        {
                            updateApi();}
                    }


                }
            }
        });
    }

    private boolean validationCheck() {


           String phoneNo = dr_number.getText().toString().trim();
        String email = dr_email.getText().toString().trim();


            if (phoneNo.isEmpty()) {
            GeneralFunction.showSnackBar(DocProfileActivity.this, parent_lay, "Please enter contact number");
            return false;
        } else if (phoneNo.length()<10) {
            GeneralFunction.showSnackBar(DocProfileActivity.this, parent_lay, "Please enter valid contact number");
            return false;
        }
        if (email.isEmpty()) {
            GeneralFunction.showSnackBar(DocProfileActivity.this, parent_lay, "Please enter email");
            return false;
        }
        else if (!email.isEmpty()&&!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            GeneralFunction.showSnackBar(DocProfileActivity.this, parent_lay, "Please enter valid email");

            return false;
        }
        if (dr_address.getText().toString().isEmpty()) {
            GeneralFunction.showSnackBar(DocProfileActivity.this, parent_lay, "Please enter address");
            return false;
        }



        return true;
    }


    private void editDoctorWithImageApi() {
        GeneralFunction.showProgress(DocProfileActivity.this);

        MultipartBody.Part imageUser = null;
        if (resultUri != null) {
            final RequestBody reqFile;
            try {
                reqFile = RequestBody.create(MediaType.parse("image/*"), new File(new URI(resultUri.toString())));
                imageUser = MultipartBody.Part.createFormData("image", new File(new URI(resultUri.toString())).getName(), reqFile);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

        }

        RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(doctorLoginOutput.userDetail.userId));
        RequestBody phonenumber = RequestBody.create(MediaType.parse("text/plain"), dr_number.getText().toString().trim());
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), dr_email.getText().toString());
        RequestBody address = RequestBody.create(MediaType.parse("text/plain"), dr_address.getText().toString());



        RestClient.getModalApiService().apiUpdateWithImage(imageUser, user_id, phonenumber,email, address).enqueue(new Callback<DoctorLoginOutput>() {
            @Override
            public void onResponse(Call<DoctorLoginOutput> call, Response<DoctorLoginOutput> response) {


                GeneralFunction.dismissProgress();

                if(response.body().status==1)
                {
                    Toast.makeText(DocProfileActivity.this, "Profile updated successfully", Toast.LENGTH_SHORT).show();
                    dr_number.setEnabled(false);
                    Prefs prefs = Prefs.with(DocProfileActivity.this);
                    prefs.save(Constant.USER_DATA, response.body());

                    dr_email.setEnabled(false);
                    dr_address.setEnabled(false);
                    update_btn.setVisibility(View.GONE);
                    select_txtview.setVisibility(View.VISIBLE);

              //      finish();
                }
                else
                {
                    GeneralFunction.showSnackBar(DocProfileActivity.this,parent_lay,response.body().message);
                }

                Log.e("res", "" + new Gson().toJson(response.body()));
            }

            @Override
            public void onFailure(Call<DoctorLoginOutput> call, Throwable t) {
                //  commonFunction.hideProgressDialog();
                GeneralFunction.dismissProgress();
            }
        });

    }
    private void updateApi() {

        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("user_id", String.valueOf(doctorLoginOutput.userDetail.userId));
        hashMap.put("phonenumber",dr_number.getText().toString().trim());
        hashMap.put("email",dr_email.getText().toString());
        hashMap.put("address",dr_address.getText().toString());

        Log.e("login params",""+hashMap);

        GeneralFunction.showProgress(DocProfileActivity.this);
        RestClient.getModalApiService().apiDoctorUpdate(hashMap).enqueue(new Callback<DoctorLoginOutput>() {
            @Override
            public void onResponse(Call<DoctorLoginOutput> call, Response<DoctorLoginOutput> response) {
                GeneralFunction.dismissProgress();
                if (response.isSuccessful()) {
                    if(response.body().status==1)
                    {
                              Toast.makeText(DocProfileActivity.this, "Profile updated successfully", Toast.LENGTH_SHORT).show();
                        Prefs prefs = Prefs.with(DocProfileActivity.this);
                        prefs.save(Constant.USER_DATA, response.body());
                        dr_number.setEnabled(false);

                        dr_email.setEnabled(false);
                        dr_address.setEnabled(false);
                        update_btn.setVisibility(View.GONE);
                        select_txtview.setVisibility(View.VISIBLE);

                        Log.e("login res",""+new Gson().toJson(response.body()));
                    }
                    else
                    {
                        GeneralFunction.showSnackBar(DocProfileActivity.this,parent_lay,response.body().message);
                            }


                }
                else {
                    GeneralFunction.dismissProgress();

                }
            }

            @Override
            public void onFailure(Call<DoctorLoginOutput> call, Throwable t) {
                //  if(view!=null) {
                GeneralFunction.dismissProgress();
                //   view.signupFailure(t.getMessage());
                // }
            }

        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1213 && resultCode == Activity.RESULT_OK) {
          /*  String filePath = data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH);
            Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
            profile_img.setImageBitmap(selectedImage);*/
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                //  String filePath = data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH);
                resultUri = result.getUri();
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                profile_img.setImageBitmap(bitmap);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        blockStatusApi();
    }

    private void blockStatusApi() {

        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("user_id", String.valueOf(doctorLoginOutput.userDetail.userId));



        Log.e("get patient params",""+hashMap);

        //     GeneralFunction.showProgress(HomeActivity.this);
        RestClient.getModalApiService().apiBlockStatus(hashMap).enqueue(new Callback<BlockOutput>() {
            @Override
            public void onResponse(Call<BlockOutput> call, Response<BlockOutput> response) {
                //   GeneralFunction.dismissProgress();
                if (response.isSuccessful()) {
                    Log.e("login res",""+new Gson().toJson(response.body()));


                    if(response.body().doctorStatus.equalsIgnoreCase("1"))
                    {

                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(DocProfileActivity.this);


                        //Uncomment the below code to Set the message and title from the strings.xml file
                        builder.setMessage("Your account is blocked by admin!") .setTitle("Block");

                        //Setting message manually and performing action on button click
                        builder.setMessage("Your account is blocked by admin!")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Prefs.get().removeAll();
                                        Intent intent=new Intent(DocProfileActivity.this,LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                });
                                       /* .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                //  Action for 'NO' Button
                                                dialog.cancel();
                                                Toast.makeText(getApplicationContext(),"you choose no action for alertbox",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        });*/
                        //Creating dialog box
                        android.app.AlertDialog alert = builder.create();
                        //Setting the title manually
                        alert.setTitle("Block");
                        alert.show();

                    }
                    else if(response.body().doctorStatus.equalsIgnoreCase("2"))
                    {}
                    String android_id = Settings.Secure.getString(getContentResolver(),
                            Settings.Secure.ANDROID_ID);
                    if(response.body().device_id.equalsIgnoreCase(android_id))
                    {}
                    else
                    {
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(DocProfileActivity.this);


                        //Uncomment the below code to Set the message and title from the strings.xml file
                        builder.setMessage("Your account is refreshed by the admin. Please login again!") .setTitle("Alert!");

                        //Setting message manually and performing action on button click
                        builder.setMessage("Your account is refreshed by the admin. Please login again!")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Prefs.get().removeAll();
                                        Intent intent=new Intent(DocProfileActivity.this,LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                });
                                       /* .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                //  Action for 'NO' Button
                                                dialog.cancel();
                                                Toast.makeText(getApplicationContext(),"you choose no action for alertbox",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        });*/
                        //Creating dialog box
                        android.app.AlertDialog alert = builder.create();
                        //Setting the title manually
                        alert.setTitle("Alert!");
                        alert.show();
                    }

                }
                else {



                }
            }

            @Override
            public void onFailure(Call<BlockOutput> call, Throwable t) {

            }

        });

    }

}
