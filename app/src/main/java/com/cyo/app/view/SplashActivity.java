package com.cyo.app.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.cyo.app.R;
import com.cyo.app.model.DoctorLoginOutput;
import com.cyo.app.utils.Constant;
import com.cyo.app.utils.Prefs;


public class SplashActivity extends AppCompatActivity {
    DoctorLoginOutput doctorLoginOutput;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        doctorLoginOutput = Prefs.with(SplashActivity.this).getObject(Constant.USER_DATA, DoctorLoginOutput.class);



        /****** Create Thread that will sleep for 5 seconds****/
        Thread background = new Thread() {
            public void run() {
                try {
                    // Thread will sleep for 5 seconds
                    sleep(3 * 1000);
if(doctorLoginOutput!=null&&doctorLoginOutput.userDetail.id!=null&&!String.valueOf(doctorLoginOutput.userDetail.id).isEmpty())
{
    Intent i = new Intent(getBaseContext(), HomeActivity.class);
    startActivity(i);

}
else {
    // After 5 seconds redirect to another intent
    Intent i = new Intent(getBaseContext(), LoginActivity.class);
    startActivity(i);
}
                    //Remove activity
                    finish();
                } catch (Exception e) {
                }
            }
        };
        // start thread
        background.start();
    }

    int change(int x) {
        x = 12;
        return x;
    }
}
