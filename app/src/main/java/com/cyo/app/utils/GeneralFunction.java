package com.cyo.app.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Environment;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.cyo.app.R;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by cbl1005 on 1/1/18.
 */

public class GeneralFunction {
    private static Dialog dialog;

    public static void showSnackBar(Context context, View parentView, String msg) {
        final Snackbar snackbar = Snackbar.make(parentView, msg, Snackbar.LENGTH_SHORT);
        snackbar.setAction(context.getString(R.string.OK), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        }).show();


        //set color of action button text
        snackbar.setActionTextColor(ContextCompat.getColor(context, R.color.appColor));

        //set color of snackbar text
        TextView view = snackbar.getView().findViewById(R.id.snackbar_text);
        view.setTextColor(ContextCompat.getColor(context, android.R.color.white));
    }


    public static String getFormatFromDate(Date date, String format) {

        Locale locale = Locale.getDefault();
       /* UserData userData = Prefs.with(context).getObject("userData", UserData.class);
        if(!userData.languageID.equalsIgnoreCase("EN")) {
            //  locale = new Locale("ar","SA");
        }*/
        SimpleDateFormat sdf = new SimpleDateFormat(format, locale);
        try {
            return sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showProgress(Context activity) {

        if (dialog != null) {
            if (dialog.isShowing()) {
                return;
            }
        }
        try {
            dialog = new Dialog(activity);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.layout_progress);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void dismissProgress() {
        try {
            if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void isUserBlocked(final AlertDialog.Builder builder, final Activity activity) {
        builder.setMessage(activity.getString(R.string.token_expired));
        builder.setPositiveButton(activity.getString(R.string.OK), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Prefs.with(activity).removeAll();
              /*  Intent intent = new Intent(activity, AuthenticateActivity.class);
                activity.finishAffinity();
                activity.startActivity(intent);*/
            }

        });

        builder.setCancelable(false);
        builder.show();
    }

    public static AlertDialog isUserBlockedDialog(final Activity activity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(activity.getString(R.string.token_expired));
        builder.setPositiveButton(activity.getString(R.string.OK), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Prefs.with(activity).removeAll();
              /*  Prefs.with(activity).save(Constant.LANGUAGE_CODE, "en");    // Reset language to en in case of token expire
                Intent intent = new Intent(activity, AuthenticateActivity.class);
                activity.finishAffinity();
                activity.startActivity(intent);*/
            }

        });
        builder.setCancelable(false);

        AlertDialog dialog = builder.create();
        dialog.show();
        return dialog;
    }

    public static boolean isNetworkConnected(Activity context, View view) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null) {
            return true;
        } else {
            if (view != null) {
                showSnackBar(context, view, context.getString(R.string.check_connection));
            }
            return false;
        }
    }

    @Nullable
    public static File createImageFile(@NonNull final String directory) throws IOException {
        File imageFile = null;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            File storageDir = new File(directory);
            if (!storageDir.mkdirs()) {
                if (!storageDir.exists()) {
                    return null;
                }
            }
            String imageFileName = "IMG_" + System.currentTimeMillis() + "_";

            imageFile = File.createTempFile(imageFileName, ".jpg", storageDir);
        }
        return imageFile;
    }

    public static boolean isActivityLaunchedFromHistory(AppCompatActivity activity) {
        return (activity.getIntent().getFlags() & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) != 0;
    }
    public static String currentDate() {

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
        String formattedDate = df.format(c);
        return formattedDate;
    }

    public static String getMyDateFormat(String date, String currentFormat, String format) {
        String finalDate;
        SimpleDateFormat curFormater = new SimpleDateFormat(currentFormat, Locale.ENGLISH);
        Date dateObj = null;
        try {
            dateObj = curFormater.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat postFormater;
        postFormater = new SimpleDateFormat(format, Locale.ENGLISH);
        if (dateObj != null)
            finalDate = postFormater.format(dateObj);
        else
            finalDate = "";
        return finalDate;
    }
}
