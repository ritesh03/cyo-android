package com.cyo.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cyo.app.R;
import com.cyo.app.model.ScreeningDetailsOutput;
import com.cyo.app.view.PhotoViewActivity;

import java.util.List;

import static com.cyo.app.view.ScreenImagesActivity.delete_icon;
import static com.cyo.app.view.ScreenImagesActivity.image_ids;
import static com.cyo.app.view.ScreenImagesActivity.img_id;
import static com.cyo.app.view.ScreenImagesActivity.share_icon;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ViewHolder> {

    Context context;
    TextView select_txtview;
    List<ScreeningDetailsOutput.UserDetail_> userDetails;
    // RecyclerView recyclerView;

    boolean visibility_status;

    public ImagesAdapter(Context context, List<ScreeningDetailsOutput.UserDetail_> userDetails, boolean visibility_status, TextView select_txtview) {
        this.context = context;
        this.userDetails = userDetails;
        this.visibility_status = visibility_status;
        this.select_txtview = select_txtview;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.image_name_txt.setText(userDetails.get(position).imageName);

        Log.e("images", "" + userDetails.get(position).image);
        if (userDetails.get(position).id.equalsIgnoreCase(img_id)) {
            holder.checkbox.setChecked(true);
        } else {
            holder.checkbox.setChecked(false);
        }
        if (visibility_status) {

            holder.checkbox.setVisibility(View.VISIBLE);
        } else {
            holder.checkbox.setVisibility(View.GONE);
        }
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(5.0f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(circularProgressDrawable)
                // .error(R.drawable.user_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();
        Glide.with(context).load(userDetails.get(position).image).apply(options).into(holder.imageView);


        holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    image_ids.add(userDetails.get(position).id);
                    Log.e("if", image_ids + "");
                } else {

                    for (int i = 0; i < image_ids.size(); i++) {
                        if (userDetails.get(position).id.equalsIgnoreCase(image_ids.get(i))) {
                            image_ids.remove(i);
                            Log.e("else", image_ids + "");
                        }
                    }


                }


                if (image_ids.size() == 0) {
                    share_icon.setVisibility(View.GONE);
                    delete_icon.setVisibility(View.GONE);
                } else {
                    share_icon.setVisibility(View.VISIBLE);
                    delete_icon.setVisibility(View.VISIBLE);
                }

            }
        });
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (visibility_status) {
                    holder.checkbox.performClick();
                } else {
                    Intent intent = new Intent(context, PhotoViewActivity.class);
                    intent.putExtra("image", userDetails.get(position).image);
                    intent.putExtra("type", "");
                    intent.putExtra("image_name", userDetails.get(position).imageName.replaceAll("-copy",""));

                    intent.putExtra("patient_id", userDetails.get(position).patientId);
                    intent.putExtra("screening_no", userDetails.get(position).screeningNo);

                    context.startActivity(intent);
                }

            }
        });

        holder.imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // if (visibility_status) {
                img_id = userDetails.get(position).id;
                share_icon.setVisibility(View.VISIBLE);
                delete_icon.setVisibility(View.VISIBLE);
                image_ids.add(userDetails.get(position).id);
                select_txtview.performClick();
                select_txtview.setVisibility(View.VISIBLE);
//                visibility_status=true;
//                holder.checkbox.setChecked(true);
//                notifyDataSetChanged();
                return true;
            }
        });
    }


    @Override
    public int getItemCount() {
        return userDetails.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        CheckBox checkbox;
        TextView image_name_txt;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = (ImageView) itemView.findViewById(R.id.imageVieww);
            checkbox = itemView.findViewById(R.id.checkbox);
            image_name_txt = itemView.findViewById(R.id.image_name_txt);
        }
    }
}