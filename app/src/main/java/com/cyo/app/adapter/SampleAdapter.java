package com.cyo.app.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.codewaves.stickyheadergrid.StickyHeaderGridAdapter;
import com.cyo.app.R;
import com.cyo.app.model.ScreeningDetailsOutput;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergej Kravcenko on 4/24/2017.
 * Copyright (c) 2017 Sergej Kravcenko
 */

public class SampleAdapter extends StickyHeaderGridAdapter {
   private List<List<String>> labels;
   boolean visibility_status;
    ArrayList<ScreeningDetailsOutput.UserDetail> screening_detail_list;
   public SampleAdapter(int sections, int count, boolean visibility_status, ArrayList<ScreeningDetailsOutput.UserDetail> screening_detail_list) {
      this.visibility_status=visibility_status;
      this.screening_detail_list=screening_detail_list;
      labels = new ArrayList<>(sections);
      for (int s = 0; s < sections; ++s) {
         List<String> labels = new ArrayList<>(screening_detail_list.size());
         String datee="";
          String label = "";

         for (int i = 0; i < screening_detail_list.size(); ++i) {
             
             
             if(datee.equalsIgnoreCase(screening_detail_list.get(i).screeningDate))
             {
                Log.e("if","if");


             }
             else
             {
                Log.e("else","else");
                 datee=screening_detail_list.get(i).screeningDate;
                 label= datee;
                labels.add(label);
             }
            
            /*for (int p = 0; p < s - i; ++p) {
               label += "*\n";
            }*/

         }
         Log.e("labels",labels+"");
         this.labels.add(labels);
      }
   }

   @Override
   public int getSectionCount() {
      return labels.size();
   }

   @Override
   public int getSectionItemCount(int section) {
      return labels.get(section).size();
   }

   @Override
   public HeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent, int headerType) {
      final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_layout, parent, false);
      return new MyHeaderViewHolder(view);
   }

   @Override
   public ItemViewHolder onCreateItemViewHolder(ViewGroup parent, int itemType) {
      final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
      return new MyItemViewHolder(view);
   }

   @Override
   public void onBindHeaderViewHolder(HeaderViewHolder viewHolder, int section) {
      final MyHeaderViewHolder holder = (MyHeaderViewHolder)viewHolder;
       //final String label = "Header " + section;
       // holder.labelView.setText(screening_detail_list.get(section-1).screeningDate);
   }

   @Override
   public void onBindItemViewHolder(ItemViewHolder viewHolder, final int section, final int position) {

      final MyItemViewHolder holder = (MyItemViewHolder)viewHolder;
      if(visibility_status)
      {
         holder.checkbox.setVisibility(View.VISIBLE);
      }
      else
      {
         holder.checkbox.setVisibility(View.GONE);
      }

      final String label = labels.get(section).get(position);

   /*   holder.labelView.setText(label);
      holder.labelView.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            final int section = getAdapterPositionSection(holder.getAdapterPosition());
            final int offset = getItemSectionOffset(section, holder.getAdapterPosition());

            labels.get(section).remove(offset);
            notifySectionItemRemoved(section, offset);
            Toast.makeText(holder.labelView.getContext(), label, Toast.LENGTH_SHORT).show();
         }
      });*/
   }

   public static class MyHeaderViewHolder extends HeaderViewHolder {
      TextView labelView;

      MyHeaderViewHolder(View itemView) {
         super(itemView);
         labelView = (TextView) itemView.findViewById(R.id.date_txt);
      }
   }

   public static class MyItemViewHolder extends ItemViewHolder {
      ImageView imageView;
CheckBox checkbox;
      MyItemViewHolder(View itemView) {
         super(itemView);
         imageView =   itemView.findViewById(R.id.imageView);
         checkbox =   itemView.findViewById(R.id.checkbox);
      }
   }
}
