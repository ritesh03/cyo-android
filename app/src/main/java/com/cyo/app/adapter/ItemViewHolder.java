package com.cyo.app.adapter;

import android.view.View;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.cyo.app.R;

public class ItemViewHolder extends RecyclerView.ViewHolder{
    public ImageView imageView;
    public ItemViewHolder(View itemView) {
        super(itemView);
        imageView = itemView.findViewById(R.id.imageView);
    }
}
