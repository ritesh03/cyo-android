package com.cyo.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cyo.app.R;
import com.cyo.app.model.GetPatientOutput;
import com.cyo.app.utils.GeneralFunction;
import com.cyo.app.view.AddPatientActivity;
import com.cyo.app.view.HomeActivity;
import com.cyo.app.view.PatientDetailActivity;
import com.cyo.app.view.PhotoViewActivity;
import com.cyo.app.view.USBCameraActivity;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder>{


    Context context;
    ArrayList<GetPatientOutput.UserDetail> patient_list;
    // RecyclerView recyclerView;
    public HomeAdapter(Context context, ArrayList<GetPatientOutput.UserDetail> patient_list) {

        this.context = context;
        this.patient_list = patient_list;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_home, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {


        holder.patient_name_txt.setText(patient_list.get(position).firstname+" "+patient_list.get(position).lastname);
        if(patient_list.get(position).lastdate.equalsIgnoreCase(""))
        {
            holder.last_visit_txt.setVisibility(View.GONE);
        }
        else {
            holder.last_visit_txt.setText("Last visit: " + GeneralFunction.getMyDateFormat(patient_list.get(position).lastdate, "yyyy/MM/dd", "dd MMM, yyyy"));
        }

        Glide.with(context).load(patient_list.get(position).image).placeholder(R.drawable.placeholder_woman).into(holder.patient_img);

     holder.user_lay.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
             Intent intent=new Intent(context, PatientDetailActivity.class);
             intent.putExtra("patient_details",new Gson().toJson(patient_list.get(position)));
             context.startActivity(intent);
         }
     });
     holder.screen_add.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
             Intent intent=new Intent(context, USBCameraActivity.class);
             intent.putExtra("patient_id",patient_list.get(position).id);
             intent.putExtra("patient_details",new Gson().toJson(patient_list.get(position)));
             context.startActivity(intent);
         }
     });
    }


    @Override
    public int getItemCount() {
        return patient_list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout user_lay;
ImageView screen_add;
        @BindView(R.id.patient_img)
        CircleImageView patient_img;
        @BindView(R.id.patient_name_txt)
        TextView patient_name_txt;
        @BindView(R.id.last_visit_txt)
        TextView last_visit_txt;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.user_lay = (RelativeLayout) itemView.findViewById(R.id.user_lay);
            screen_add =  itemView.findViewById(R.id.screen_add);

        }
    }
}