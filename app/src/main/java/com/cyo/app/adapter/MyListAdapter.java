package com.cyo.app.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cyo.app.R;
import com.cyo.app.model.ScreeningDetailsOutput;
import com.cyo.app.utils.GeneralFunction;
import com.cyo.app.view.PhotoViewActivity;
import com.cyo.app.view.ScreenImagesActivity;


import java.util.ArrayList;

public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.ViewHolder> {

    Context context;
    TextView select_txtview;
    // RecyclerView recyclerView;
    boolean visibility_status;
    ArrayList<ScreeningDetailsOutput.UserDetail> screening_detail_list;

    public MyListAdapter(Context screenImagesActivity, TextView select_txtview, ArrayList<ScreeningDetailsOutput.UserDetail> screening_detail_list, boolean visibility_status) {
        this.context = screenImagesActivity;
        this.select_txtview = select_txtview;
        this.screening_detail_list = screening_detail_list;
        this.visibility_status = visibility_status;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.header_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.date_txt.setText(GeneralFunction.getMyDateFormat(screening_detail_list.get(position).screeningDate, "yyyy/MM/dd", "dd MMM, yyyy"));

        ImagesAdapter adapter = new ImagesAdapter(context, screening_detail_list.get(position).userDetails, visibility_status,select_txtview);
        holder.recyclerView_imagess.setHasFixedSize(true);
        @SuppressLint("WrongConstant") GridLayoutManager manager = new GridLayoutManager(context, 4, GridLayoutManager.VERTICAL, false);
        holder.recyclerView_imagess.setLayoutManager(manager);
        // recyclerView.setLayoutManager(new LinearLayoutManager(this));
        holder.recyclerView_imagess.setAdapter(adapter);


        //  Glide.with(context).load(images.get(position)).into(holder.imageView);
    }


    @Override
    public int getItemCount() {
        return screening_detail_list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public RecyclerView recyclerView_imagess;
        TextView date_txt;

        public ViewHolder(View itemView) {
            super(itemView);
            this.recyclerView_imagess = (RecyclerView) itemView.findViewById(R.id.recyclerView_imagess);
            date_txt = itemView.findViewById(R.id.date_txt);
        }
    }
}