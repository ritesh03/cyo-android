package com.cyo.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cyo.app.R;
import com.cyo.app.view.PhotoViewActivity;
import com.cyo.app.view.USBCameraActivity;

import java.util.ArrayList;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder>{

    Context context;

    // RecyclerView recyclerView;
    ArrayList<String> images;
    String screening_no,patient_id;
    ArrayList<String> images_name;
    public GalleryAdapter(Context context, ArrayList<String> images, ArrayList<String> images_name,String screening_no,String patient_id) {

        this.context = context;
        this.images = images;
        this.screening_no = screening_no;
        this.images_name = images_name;
        this.patient_id = patient_id;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_gallery, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

         /*  ImagesAdapter adapter = new ImagesAdapter(context );
        holder.recyclerView_imagess.setHasFixedSize(true);
        GridLayoutManager manager = new GridLayoutManager(context, 4, GridLayoutManager.VERTICAL, false);
        holder.recyclerView_imagess.setLayoutManager(manager);
        // recyclerView.setLayoutManager(new LinearLayoutManager(this));
        holder.recyclerView_imagess.setAdapter(adapter);
*/
        holder.image_name_txt.setText(images_name.get(position));
         holder.imageView.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {

                 Intent intent=new Intent(context, PhotoViewActivity.class);
                 intent.putExtra("image",images.get(position));
                 intent.putExtra("patient_id",patient_id);
                 intent.putExtra("screening_no",screening_no);
                 intent.putExtra("image_name",images_name.get(position).replaceAll("-copy",""));
                 context.startActivity(intent);

             }
         });

         Glide.with(context).load(images.get(position)).into(holder.imageView);
    }


    @Override
    public int getItemCount() {
        return images.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        TextView image_name_txt;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = (ImageView) itemView.findViewById(R.id.imageView);
            this.image_name_txt = (TextView) itemView.findViewById(R.id.image_name_txt);

        }
    }
}