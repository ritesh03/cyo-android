package com.cyo.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cyo.app.R;
import com.cyo.app.model.GetPatientOutput;
import com.cyo.app.model.GetScreeningOutput;
import com.cyo.app.utils.GeneralFunction;
import com.cyo.app.view.PatientDetailActivity;
import com.cyo.app.view.ScreenImagesActivity;

import java.util.ArrayList;

public class PreviousScreeningAdapter extends RecyclerView.Adapter<PreviousScreeningAdapter.ViewHolder>{

    ArrayList<GetScreeningOutput.UserDetail> screening_list;
    Context context;
    // RecyclerView recyclerView;
    public PreviousScreeningAdapter(Context context, ArrayList<GetScreeningOutput.UserDetail> screening_list ) {

        this.context = context;
        this.screening_list = screening_list;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_previous_screening, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
holder.screening_name.setText("Screening "+screening_list.get(position).screeningNo);
holder.screen_date.setText("("+ GeneralFunction.getMyDateFormat( screening_list.get(position).screeningDate,"yyyy/MM/dd","dd MMM, yyyy")+")");
holder.screen_lay.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent=new Intent(context, ScreenImagesActivity.class);
        intent.putExtra("patient_id",screening_list.get(position).patientId);
        intent.putExtra("patient_email",screening_list.get(position).email);
        intent.putExtra("fname",screening_list.get(position).firstname);
        intent.putExtra("lname",screening_list.get(position).lastname);
        intent.putExtra("age",screening_list.get(position).age);
        intent.putExtra("mobile",screening_list.get(position).mobile);
        intent.putExtra("gender",screening_list.get(position).gender);
        intent.putExtra("screening_id",screening_list.get(position).id);
        intent.putExtra("screening_no",screening_list.get(position).screeningNo);
        intent.putExtra("patient_name",screening_list.get(position).firstname+" "+screening_list.get(position).lastname);
        context.startActivity(intent);
    }
});
    }


    @Override
    public int getItemCount() {
        return screening_list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView screening_name,screen_date;
RelativeLayout screen_lay;
        public ViewHolder(View itemView) {
            super(itemView);
           screening_name = (TextView) itemView.findViewById(R.id.screening_name);
            screen_lay =  itemView.findViewById(R.id.screen_lay);
            screen_date =  itemView.findViewById(R.id.screen_date);

        }
    }
}