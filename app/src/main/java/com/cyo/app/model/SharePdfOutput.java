package com.cyo.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SharePdfOutput {
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("pdf_path")
    @Expose
    public String pdfPath;
    @SerializedName("pdf_name")
    @Expose
    public String pdfName;
}
