package com.cyo.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetScreeningOutput {
    @SerializedName("status")
    @Expose
    public Integer status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("user_details")
    @Expose
    public List<UserDetail> userDetails = null;

    public class UserDetail {

        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("user_id")
        @Expose
        public Integer userId;
        @SerializedName("patient_id")
        @Expose
        public String patientId;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("screening_date")
        @Expose
        public String screeningDate;
        @SerializedName("screening_no")
        @Expose
        public String screeningNo;
        @SerializedName("firstname")
        @Expose
        public String firstname;
        @SerializedName("lastname")
        @Expose
        public String lastname;
        @SerializedName("gender")
        @Expose
        public String gender;
        @SerializedName("age")
        @Expose
        public String age;
        @SerializedName("mobile")
        @Expose
        public String mobile;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("updated_at")
        @Expose
        public String updatedAt;

    }
}
