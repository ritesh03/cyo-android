package com.cyo.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetPatientOutput {
    @SerializedName("status")
    @Expose
    public Integer status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("user_details")
    @Expose
    public List<UserDetail> userDetails = null;



    public class UserDetail {

        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("user_id")
        @Expose
        public Integer userId;
        @SerializedName("firstname")
        @Expose
        public String firstname;
        @SerializedName("lastname")
        @Expose
        public String lastname;
        @SerializedName("gender")
        @Expose
        public String gender;
        @SerializedName("mobile")
        @Expose
        public String mobile;
        @SerializedName("age")
        @Expose
        public String age;
        @SerializedName("last_screening_id")
        @Expose
        public String last_screening_id;
        @SerializedName("image")
        @Expose
        public String image;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("updataed_at")
        @Expose
        public String updataedAt;
        @SerializedName("lastdate")
        @Expose
        public String lastdate;
        @SerializedName("previous_screening_id")
        @Expose
        public String previousScreeningId;
        @SerializedName("next_screening_id")
        @Expose
        public String nextScreeningId;
        @SerializedName("next_screening_name")
        @Expose
        public String nextScreeningName;

    }
}
