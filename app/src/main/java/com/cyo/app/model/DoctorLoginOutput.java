package com.cyo.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DoctorLoginOutput {
    @SerializedName("status")
    @Expose
    public Integer status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("user_detail")
    @Expose
    public UserDetail userDetail;
    public class UserDetail {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("user_id")
        @Expose
        public String userId;
        @SerializedName("image")
        @Expose
        public String image;
        @SerializedName("firstname")
        @Expose
        public String firstname;
        @SerializedName("lastname")
        @Expose
        public String lastname;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("phonenumber")
        @Expose
        public String phonenumber;
        @SerializedName("city")
        @Expose
        public String city;
        @SerializedName("address")
        @Expose
        public String address;
        @SerializedName("gender")
        @Expose
        public String gender;
        @SerializedName("account_type")
        @Expose
        public Integer accountType;
        @SerializedName("specialization")
        @Expose
        public String specialization;
        @SerializedName("status")
        @Expose
        public String status;
        @SerializedName("block_status")
        @Expose
        public String blockStatus;
        @SerializedName("certification_number")
        @Expose
        public String certificationNumber;
        @SerializedName("dealer_name")
        @Expose
        public String dealerName;
        @SerializedName("device_id")
        @Expose
        public String deviceId;
        @SerializedName("device_type")
        @Expose
        public String deviceType;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("updated_at")
        @Expose
        public String updatedAt;

    }
}
