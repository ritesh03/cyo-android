package com.cyo.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BlockOutput {
    @SerializedName("status")
    @Expose
    public Integer status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("doctor_status")
    @Expose
    public String doctorStatus;
    @SerializedName("device_id")
    @Expose
    public String device_id;
}
