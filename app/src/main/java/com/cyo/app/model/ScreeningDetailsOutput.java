package com.cyo.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ScreeningDetailsOutput {
    @SerializedName("status")
    @Expose
    public Integer status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("user_details")
    @Expose
    public List<UserDetail> userDetails = null;


    public class UserDetail {

        @SerializedName("screening_date")
        @Expose
        public String screeningDate;
        @SerializedName("user_details")
        @Expose
        public List<UserDetail_> userDetails = null;

    }
    public class UserDetail_ {

        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("screening_id")
        @Expose
        public Integer screeningId;
        @SerializedName("user_id")
        @Expose
        public Integer userId;
        @SerializedName("patient_id")
        @Expose
        public String patientId;
        @SerializedName("screening_no")
        @Expose
        public String screeningNo;
        @SerializedName("screening_date")
        @Expose
        public String screeningDate;
        @SerializedName("image_name")
        @Expose
        public String imageName;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("image")
        @Expose
        public String image;

    }
}
