package com.cyo.app.Config;

import android.app.Activity;

import com.cyo.app.R;
import com.cyo.app.utils.DialogPopup;


public class ErrorCodes {

    public static void checkCode(Activity activity, int errorCode, String message) {

        if (errorCode == ApiResponseFlags.OK.getOrdinal()) {
            new DialogPopup().alertPopup(activity, activity.getResources().getString(R.string.dialog_alert), message, "Error");
        } else if (errorCode == ApiResponseFlags.BAD_REQUEST.getOrdinal()) {
            new DialogPopup().alertPopup(activity, activity.getResources().getString(R.string.dialog_alert), message, "Error");
        } else if (errorCode == ApiResponseFlags.UNAUTHORIZED.getOrdinal()) {
            new DialogPopup().alertPopup(activity, activity.getResources().getString(R.string.dialog_alert), message, "Logout");
        } else if (errorCode == ApiResponseFlags.USER_NOT_FOUND.getOrdinal()) {
            new DialogPopup().alertPopup(activity, activity.getResources().getString(R.string.dialog_alert), message, "Error");
        } else if (errorCode == ApiResponseFlags.USER_ALREADY_CHECKED_IN.getOrdinal()) {
            new DialogPopup().alertPopup(activity, activity.getResources().getString(R.string.dialog_alert), message, "Error");
        } else if (errorCode == ApiResponseFlags.INTERNAL_SERVER_ERROR.getOrdinal()) {
            new DialogPopup().alertPopup(activity, activity.getResources().getString(R.string.dialog_alert), message, "Error");
        } else if (errorCode == ApiResponseFlags.No_services_on_this_location_found.getOrdinal()) {
            new DialogPopup().alertPopup(activity, activity.getResources().getString(R.string.dialog_alert), message, "Error");
        } else if (errorCode == ApiResponseFlags.Created.getOrdinal()) {
            new DialogPopup().alertPopup(activity, activity.getResources().getString(R.string.dialog_alert), message, "Error");
        } else if (errorCode == ApiResponseFlags.Customer_Blocked.getOrdinal()) {
            new DialogPopup().alertPopup(activity, activity.getResources().getString(R.string.dialog_alert), message, "Error");
        }
    }


    public enum ApiResponseFlags {
        Customer_Blocked(403),
        Created(201),
        No_services_on_this_location_found(204),
        OK(200),
        BAD_REQUEST(400),
        UNAUTHORIZED(401),
        USER_NOT_FOUND(404),
        USER_ALREADY_CHECKED_IN(409),
        INTERNAL_SERVER_ERROR(500);

        private int ordinal;

        ApiResponseFlags(int ordinal) {
            this.ordinal = ordinal;
        }

        public int getOrdinal() {
            return ordinal;
        }
    }

}