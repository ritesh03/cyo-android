package com.cyo.app.Config;


/**
 * credential used for creating dev, test and live build
 */
public class Config {

    static String PAYMENT_KEY ="" ;
    public static String TRANSACTION_ID ="" ;
    public static String CARD_LAST_DIGITS ="" ;
    static String BASE_URL = "";
    static String PAYTAB_URL="https://www.paytabs.com/";
    static AppMode appMode = AppMode.LIVE;
    static String mapKey = "";

    static public String getBaseURL() {
        init(appMode);
        return BASE_URL;
    }

    static public String getPaymentKey() {
        init(appMode);
        return PAYMENT_KEY;
    }
    static public String getPaytabUrl() {
        init(appMode);
        return PAYTAB_URL;
    }


    static public String getMapkey() {
        init(appMode);
        return mapKey;
    }


    /**
     * Initialize all the variable in this method
     *
     * @param appMode
     */
    public static void init(AppMode appMode) {
        switch (appMode) {
            case DEV:
                BASE_URL = "";
                mapKey = "";
                break;

            case TEST:
                BASE_URL = "http://119.81.1.69/~callmyma/cyo/api/";
                mapKey = "";
                break;

            case LIVE:
                BASE_URL = "http://119.81.1.69/~callmyma/cyo/api/";
                mapKey = "";


                break;
        }
    }

    public enum AppMode {
        DEV, TEST, LIVE
    }

}