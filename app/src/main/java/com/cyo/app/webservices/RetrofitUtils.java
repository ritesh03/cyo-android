package com.cyo.app.webservices;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.Buffer;


public class RetrofitUtils {

    private static final String MIME_TYPE_TEXT = "text/plain";
    private static final String MIME_TYPE_IMAGE = "image/*";

    public static RequestBody StringtoRequestBody(String string){
        return RequestBody.create(MediaType.parse(MIME_TYPE_TEXT), string);
    }

    public static RequestBody ImagetoRequestBody(File file){
        return RequestBody.create(MediaType.parse(MIME_TYPE_IMAGE), file);
    }

    public static String bodyToString(final RequestBody request) {
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            if (copy != null)
                copy.writeTo(buffer);
            else
                return "";
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }

}