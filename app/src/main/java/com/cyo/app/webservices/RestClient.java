package com.cyo.app.webservices;


import com.cyo.app.Config.Config;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by cbl81 on 8/11/17.
 */

public class RestClient {

    private static ApiService apiServiceModel = null;
    private static ApiService apiServiceModelPaytab = null;

    /**
     * By using this method your response will be coming in  modal class object
     *
     * @return object of ApiService interface
     */
    public static ApiService getModalApiService() {
        if (apiServiceModel == null) {

            OkHttpClient okHttpClient = new OkHttpClient();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Config.getBaseURL())
                    // .client(fetchHeaders(context))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient.newBuilder().connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).writeTimeout(60, TimeUnit.SECONDS).build())
                    .build();

            apiServiceModel =
                    retrofit.create(ApiService.class);
        }
        return apiServiceModel;
    }


    public static ApiService getPayTabApiService() {
        if (apiServiceModelPaytab == null) {

            OkHttpClient okHttpClient = new OkHttpClient();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Config.getPaytabUrl())
                    // .client(fetchHeaders(context))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient.newBuilder().connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).writeTimeout(60, TimeUnit.SECONDS).build())
                    .build();

            apiServiceModelPaytab =
                    retrofit.create(ApiService.class);
        }
        return apiServiceModelPaytab;
    }
}
