package com.cyo.app.webservices;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.cyo.app.Config.Config;
import com.google.gson.Gson;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class Send_image_private_service {
    private URL connectURL;
    private String group_id, image1, image2;
    private String response;
    byte[] dataToServer;
    Context ctx;
    FileInputStream fs;
    String receiver_id;
    SharedPreferences sp;
    String patient_id, user_id, screening_id,screening_date,image_name;
    private String existingfilename;
    private String existingfilename2;
    ArrayList<String> images;

    public Send_image_private_service(ArrayList<String> images, Context c, String patient_id, String user_id, String screening_id, String screening_date, String image_name) {
        Log.e("print", image1 + " " + image2 + "  ");
        try {
            Log.e("Enter try connection", "Enter the try connection");

            connectURL = new URL(Config.getBaseURL() + "addscreening");

        } catch (Exception ex) {
            Log.i("URL FORMATION", "MALFORMATED URL");
        }

        this.patient_id = patient_id;


        this.user_id = user_id;
        this.screening_id = screening_id;
        this.image_name = image_name;
        this.screening_date = screening_date;
        this.images = images;

        this.ctx = c;
        /*    File f = new File(upload_file);
            try {
                fs=new FileInputStream(f.getAbsolutePath());
            } catch (FileNotFoundException e) {

                e.printStackTrace();
            }*/


    }

    public String doStart() {
        fileInputStream = fs;
        fileInputStream1 = fs;
        Log.e("Enter the dostart", "Enter the dostart");
        return thirdTry();
    }

    FileInputStream fileInputStream = null;
    FileInputStream fileInputStream1 = null;

    public String thirdTry() {
        Log.e("Enter the thirdTry", "Enter the thirdTry");
        String exsistingFileName = "";
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        String Tag = "3rd";
        try {

            Gson gson = new Gson();


            HttpURLConnection conn = (HttpURLConnection) connectURL.openConnection();
            conn.setDoInput(true);
            conn.setConnectTimeout(20000);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

              /*  SharedPreferences sp=ctx.getSharedPreferences(Globalconstants.pref, Context.MODE_PRIVATE);
                String sender_id=sp.getString(Globalconstants.User_id, "");
             */
            DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
            dos.writeBytes(twoHyphens + boundary + lineEnd);

            //---------countrycode

            dos.writeBytes("Content-Disposition: form-data; name=\"user_id\"" + lineEnd);
            dos.writeBytes("Content-Type: text/plain;charset=UTF-8" + lineEnd);
            dos.writeBytes(lineEnd);
            dos.writeBytes(user_id + lineEnd);
           /* dos.writeBytes(URLEncoder.encode(
                   user_id,
                    java.nio.charset.StandardCharsets.UTF_8.toString()
            ) + lineEnd);*/
            dos.writeBytes(twoHyphens + boundary + lineEnd);

//                Log.d("SignupPLog",gson.toJson(VerificationActivity.detail));
            Log.e("user_id",user_id);
            //------Mobile

            dos.writeBytes("Content-Disposition: form-data; name=\"patient_id\"" + lineEnd);
            dos.writeBytes("Content-Type: text/plain;charset=UTF-8" + lineEnd);
            dos.writeBytes(lineEnd);
            dos.writeBytes(patient_id + lineEnd);
            dos.writeBytes(twoHyphens + boundary + lineEnd);

          //  Log.e("patient_id",patient_id);

            //------about

            dos.writeBytes("Content-Disposition: form-data; name=\"screening_no\"" + lineEnd);
            dos.writeBytes("Content-Type: text/plain;charset=UTF-8" + lineEnd);
            dos.writeBytes(lineEnd);
            dos.writeBytes(screening_id + lineEnd);
            dos.writeBytes(twoHyphens + boundary + lineEnd);


            Log.e("screening_id",screening_id);
            //------education

            dos.writeBytes("Content-Disposition: form-data; name=\"screening_date\"" + lineEnd);
            dos.writeBytes("Content-Type: text/plain;charset=UTF-8" + lineEnd);
            dos.writeBytes(lineEnd);
            dos.writeBytes(screening_date + lineEnd);
            dos.writeBytes(twoHyphens + boundary + lineEnd);

            Log.d("screening_date",screening_date);

            //------education

            dos.writeBytes("Content-Disposition: form-data; name=\"image_name\"" + lineEnd);
            dos.writeBytes("Content-Type: text/plain;charset=UTF-8" + lineEnd);
            dos.writeBytes(lineEnd);
            dos.writeBytes(image_name + lineEnd);
            dos.writeBytes(twoHyphens + boundary + lineEnd);

            Log.d("screening_date",screening_date);




//--------------image
            for (int i = 0; i < images.size(); i++)
            {
                Log.e("Enter the for loop", "Enter the forloop");

                String key="";
                String path="";


                    path=images.get(i);

                File f = new File(path);
                exsistingFileName = f.getName();
                Log.e("exsistingFileName", exsistingFileName);
                fileInputStream = new FileInputStream(f);
                //Log.e("KEYYYYYSSSS", aa[i]);

                dos.writeBytes("Content-Disposition: form-data; name=\"image[]"+"\";filename=\""+ exsistingFileName + "\"" + lineEnd);
                if (exsistingFileName.endsWith(".jpg")) {
                    Log.i("imagetype", "1");
                    dos.writeBytes("Content-type: image/jpg;" + lineEnd);
                }
                if (exsistingFileName.endsWith(".png")) {
                    Log.i("imagetype", "2");
                    dos.writeBytes("Content-type: image/png;" + lineEnd);
                }
                if (exsistingFileName.endsWith(".gif")) {
                    Log.i("imagetype", "3");
                    dos.writeBytes("Content-type: image/gif;" + lineEnd);
                }
                if (exsistingFileName.endsWith(".jpeg")) {
                    Log.i("imagetype", "4");
                    dos.writeBytes("Content-type: image/jpeg;" + lineEnd);
                }
                if (exsistingFileName.endsWith(".mp4")) {
                    Log.i("videotype", "1");
                    dos.writeBytes("Content-type: video/mp4;" + lineEnd);
                }
                if (exsistingFileName.endsWith(".avi")) {
                    Log.i("videotype", "2");
                    dos.writeBytes("Content-type: video/avi;" + lineEnd);
                }
                if (exsistingFileName.endsWith(".ogg")) {
                    Log.i("videotype", "3");
                    dos.writeBytes("Content-type: video/ogg;" + lineEnd);
                }
                if (exsistingFileName.endsWith(".3gp")) {
                    Log.i("videotype", "4");
                    dos.writeBytes("Content-type: video/3gp;" + lineEnd);
                }
                dos.writeBytes(lineEnd);

                int bytesAvailable = fileInputStream.available();
                int maxBufferSize = 1024*1024;
                int bufferSize = Math.min(bytesAvailable, maxBufferSize);
                byte[] buffer = new byte[bufferSize];

                int bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                if(i< images.size()){
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    //dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                    Log.e("VALUE",""+i);
                }
                else
                {
                    if(i+1==images.size())
                    {
                        dos.writeBytes(lineEnd);
                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                        Log.e("VALUE",""+i);
                    }
                    else
                    {
                        dos.writeBytes(lineEnd);
                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        Log.e("VALUE",""+i);

                    }

                }
                Log.e("file Strem",""+fileInputStream);
                fileInputStream.close();
                dos.flush();
                //dos.close();


            }

            dos.close();

            Log.e(Tag, "File is written");

            InputStream is = conn.getInputStream();

            int ch;

            StringBuffer b = new StringBuffer();
            while ((ch = is.read()) != -1) {
                b.append((char) ch);
            }
            String s = b.toString();



            Log.e("Response of Add pics", s);
            response = s;

         //   imageCertificate=null;
         //   commonFuction.hideProgressDialog();
        } catch (MalformedURLException ex) {
           // commonFuction.hideProgressDialog();
            // Toast.makeText(frofileThreeContext, "Something wend wrong", Toast.LENGTH_SHORT).show();
            Log.e(Tag, "error: " + ex.getMessage(), ex);
        } catch (IOException ioe) {
          //  commonFuction.hideProgressDialog();
            //     Toast.makeText(frofileThreeContext, "Something wend wrong", Toast.LENGTH_SHORT).show();
            Log.e(Tag, "error3: " + ioe.getMessage(), ioe);
        }
        return response;
    }

}
