package com.cyo.app.webservices;


import com.cyo.app.model.AddPatientOutput;
import com.cyo.app.model.BlockOutput;
import com.cyo.app.model.DoctorLoginOutput;
import com.cyo.app.model.GetPatientOutput;
import com.cyo.app.model.GetScreeningOutput;
import com.cyo.app.model.ScreeningDetailsOutput;
import com.cyo.app.model.SharePdfOutput;
import com.cyo.app.view.AddPatientActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by cbl81 on 8/11/17.
 */

public interface ApiService {

    @FormUrlEncoded
    @POST("login_doctor")
    Call<DoctorLoginOutput> apiDoctorLogin(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @POST("update_doctor")
    Call<DoctorLoginOutput> apiDoctorUpdate(@FieldMap HashMap<String, String> map);

    @Multipart
    @POST("update_doctor")
    Call<DoctorLoginOutput> apiUpdateWithImage(@Part MultipartBody.Part image,
                                                  @Part("user_id") RequestBody user_id,
                                                  @Part("phonenumber") RequestBody phonenumber,
                                                  @Part("email") RequestBody email,
                                                  @Part("address") RequestBody address

    );

    @FormUrlEncoded
    @POST("patient_list")
    Call<GetPatientOutput> apiGetPatient(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @POST("getscreening")
    Call<GetScreeningOutput> apiGetScreening(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @POST("screening_details")
    Call<ScreeningDetailsOutput> apiScreenDetails(@FieldMap HashMap<String, String> map);
    @FormUrlEncoded
    @POST("delete_screening")
    Call<AddPatientOutput> apiDeleteImages(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @POST("patient_search")
    Call<GetPatientOutput> apiSearchPatient(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @POST("add_patient")
    Call<AddPatientOutput> apiAddPatient(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @POST("feedback")
    Call<AddPatientOutput> apiFeedback(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @POST("update_patient")
    Call<AddPatientOutput> apiUpdate_patient(@FieldMap HashMap<String, String> map);
    @FormUrlEncoded
    @POST("mail_pdf")
    Call<AddPatientOutput> apiMailPdf(@FieldMap HashMap<String, String> map);
    @FormUrlEncoded
    @POST("block_screening")
    Call<BlockOutput> apiBlockStatus(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @POST("share_pdf")
    Call<SharePdfOutput> apiSharePdf(@FieldMap HashMap<String, String> map);

    @Multipart
    @POST("add_patient")
    Call<AddPatientOutput> apiAddPatientWithImage(@Part MultipartBody.Part image,
                                           @Part("user_id") RequestBody user_id,
                                           @Part("firstname") RequestBody firstname,
                                           @Part("lastname") RequestBody lastname,
                                           @Part("gender") RequestBody gender,
                                           @Part("email") RequestBody email,
                                           @Part("mobile") RequestBody mobile,
                                           @Part("age") RequestBody age
    );

    @Multipart
    @POST("update_patient")
    Call<AddPatientOutput> apiUpdatePatientWithImage(@Part MultipartBody.Part image,
                                           @Part("user_id") RequestBody user_id,
                                           @Part("patient_id") RequestBody patient_id,
                                           @Part("firstname") RequestBody firstname,
                                           @Part("lastname") RequestBody lastname,
                                           @Part("gender") RequestBody gender,
                                           @Part("email") RequestBody email,
                                           @Part("mobile") RequestBody mobile,
                                           @Part("age") RequestBody age
    );


    @Multipart
    @POST("addscreening")
    Call<AddPatientOutput> apiAddScreening(@Part MultipartBody.Part[] image,


                                           @Part("user_id") RequestBody user_id,
                                           @Part("patient_id") RequestBody patient_id,
                                           @Part("screening_no") RequestBody screening_no,
                                           @Part("screening_date") RequestBody screening_date

    );
    @Multipart
    @POST("addscreening")
    Call<AddPatientOutput> postFile(@PartMap Map<String,RequestBody> image,  @Part("image_name[]") RequestBody[] image_name, @Part("user_id") String user_id, @Part("patient_id") String patient_id, @Part("screening_no") String screening_no, @Part("screening_date") String screening_date);

    @FormUrlEncoded
    @POST("verify_otp_doctor")
    Call<DoctorLoginOutput> apiOtpVerify(@FieldMap HashMap<String, String> map);

}
