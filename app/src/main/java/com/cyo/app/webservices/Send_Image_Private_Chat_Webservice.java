package com.cyo.app.webservices;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.cyo.app.model.AddPatientOutput;
import com.cyo.app.utils.GeneralFunction;
import com.google.gson.Gson;

import java.util.ArrayList;


public class Send_Image_Private_Chat_Webservice {

    Context context;

    //ProgressDialog pdia;
    Send_image_private_service addMenuWebService;

    public void sendEditPostRequest(String type,final ArrayList<String> images, final Context c, final String patient_id, final String user_id, final String screening_id, final String screening_date, final String image_name, final RelativeLayout parent_lay) {
        context = c;

        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {

            protected void onPreExecute() {
                super.onPreExecute();
               // GeneralFunction.showProgress(context);
			/*	 pdia = ProgressDialog.show(c, " ", "Please Wait.");
				 pdia.setCanceledOnTouchOutside(false);
*/
			if(type.equalsIgnoreCase("edit"))
            {
                GeneralFunction.showProgress(c);
            }
            }

            @Override
            protected String doInBackground(String... params) {


                addMenuWebService = new  Send_image_private_service(images , context,patient_id,user_id,screening_id,screening_date,image_name);
                String result = addMenuWebService.doStart();

                return result;

            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
           //     GeneralFunction.dismissProgress();
              // Log.e("SignupResponse",result);


                try {

                    Gson gson = new Gson();
                    AddPatientOutput doctorLoginResult = gson.fromJson(result, AddPatientOutput.class);
                 if( doctorLoginResult.status==1)
                 {

                  //   Toast.makeText(c, "Screening added successfully", Toast.LENGTH_SHORT).show();
                 }
                 else
                 {
                     GeneralFunction.showSnackBar(context,parent_lay,doctorLoginResult.message);
                 }


                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

        }
        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute();

    }


}
